<?php

namespace Database\Seeders;

use App\Models\GlobalInventory;
use App\Models\GlobalVendor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class GlobalVendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        GlobalVendor::insert([
           ['name' => 'Amazon', 'url' => 'amazon.com'],
           ['name' => 'AliExpress', 'url' => 'aliexpress.com']
        ]);
    }
}
