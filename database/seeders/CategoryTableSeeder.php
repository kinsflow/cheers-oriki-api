<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Category::insert([
           ['title' => 'auto', 'description' => 'auto description'],
           ['title' => 'phone', 'description' => 'phone description'],
           ['title' => 'computer', 'description' => 'computer description'],
           ['title' => 'gym', 'description' => 'gym description'],
           ['title' => 'food', 'description' => 'food description']
        ]);
    }
}
