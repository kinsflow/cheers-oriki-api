<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Admin::create([
            'first_name' => 'super',
            'last_name' => 'admin',
            'email' => 'admin@cheers.global',
            'email_verified_at' => now(),
            'password' => bcrypt('cheers')
        ]);
    }
}
