<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->unique(['user_id', 'email', 'phone'], 'user_email_phone_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->index('user_id', 'user_id_foreign');
            $table->index('email', 'email_foreign');
            $table->index('phone', 'phone_foreign');
            $table->dropUnique('user_email_phone_unique');
        });
    }
}
