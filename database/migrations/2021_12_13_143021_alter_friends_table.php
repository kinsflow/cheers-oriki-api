<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('friends', function (Blueprint $table){
            $table->unique(['first_user', 'second_user', 'acted_user'], 'first_second_acted_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('friends', function (Blueprint $table){
            $table->index('first_user', 'first_user_foreign');
            $table->index('second_user', 'second_user_foreign');
            $table->index('acted_user', 'acted_user_foreign');
            $table->dropUnique('first_second_acted_unique');
        });
    }
}
