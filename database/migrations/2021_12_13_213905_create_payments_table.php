<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->uuid('id');
            $table->foreignId('user_id');
            $table->foreignId('wallet_id');
            $table->decimal('amount', 15);
            $table->dateTime('paid_at')->nullable();
            $table->string('reference')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('status')->nullable();
            $table->enum('transaction_action_type', ['fund_wallet_action', 'withdraw_wallet_action']);
            $table->string("account_bank")->nullable(); // for withdrawal
            $table->string("account_number")->nullable(); // for withdrawal
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
