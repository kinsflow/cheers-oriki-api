<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('first_user');
            $table->unsignedBigInteger('second_user');
            $table->unsignedBigInteger('acted_user');
            $table->enum('status', ['pending', 'confirmed', 'blocked']);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('first_user')->on('users')->references('id')->cascadeOnDelete();
            $table->foreign('second_user')->on('users')->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
