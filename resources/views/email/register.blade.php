
<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
  <div style="margin:50px auto;width:70%;padding:20px 0">
    <div style="border-bottom:1px solid #eee">
      <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Cheers App</a>
    </div>
    <p style="font-size:1.1em">Hi {{ $data["first_name"] }}</p>
    <p>Welcome to Cheers. We are glad to helped you achieve your memorable dreams. Here is your login credentials</p>
    <h3>Username: {{ $data["username"] }}</h3>
{{--    <h3>Passowrd: {{ $data["password"] }}</h3>--}}
    <br>
    <p style="font-size:0.9em;">Regards,<br />Cheers App</p>
    <hr style="border:none;border-top:1px solid #eee" />
    <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
      <p>Cheers App</p>
      <p>1600 Amphitheatre Parkway</p>
      <p>California</p>
    </div>
  </div>
</div>
