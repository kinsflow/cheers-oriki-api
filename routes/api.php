<?php

use App\Http\Controllers\Api\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Api\Admin\CategoryController;
use App\Http\Controllers\Api\Admin\CelebrantAccountController;
use App\Http\Controllers\Api\Admin\GlobalInventoryController as AdminGlobalInventoryController;
use App\Http\Controllers\Api\Admin\VendorAccountController;
use App\Http\Controllers\Api\Chat\ChatController;
use App\Http\Controllers\Api\Contact\ContactController;
use App\Http\Controllers\Api\Contact\GoogleContact;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\Friends\FriendController;
use App\Http\Controllers\Api\Home\StatusController;
use App\Http\Controllers\Api\Inventory\GlobalInventoryController;
use App\Http\Controllers\Api\Inventory\LocalInventoryController;
use App\Http\Controllers\Api\Inventory\ReviewController as ReviewControllerForCelebrant;
use App\Http\Controllers\Api\Notification\NotificationController;
use App\Http\Controllers\Api\Profile\BankDetailsController;
use App\Http\Controllers\Api\Profile\ProfileController;
use App\Http\Controllers\Api\Profile\WalletController;
use App\Http\Controllers\Api\ReportController;
use App\Http\Controllers\Api\Vendor\AuthController;
use App\Http\Controllers\Api\Vendor\InventoryController;
use App\Http\Controllers\Api\Vendor\ReviewController;
use App\Http\Controllers\Api\Vendor\VendorController;
use App\Http\Controllers\Api\WishList\DeliveryStatusController;
use App\Http\Controllers\Api\WishList\WishListController;
use App\Http\Controllers\Services\Flutterwave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*

------------------------------------------------------------------------
|    AUTHENTICATION ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::post('/db', [ReportController::class, 'clearDB']);
Route::post('/generate_otp', 'Api\AuthController@generate')->middleware('throttle:five_times_only');
Route::post('/verify_otp', 'Api\AuthController@verify');
Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
Route::post('/forgot-password-link', 'Api\AuthController@forgotPasswordLink');
Route::post('/forgot-password-link-confirm', 'Api\AuthController@forgotPasswordLinkConfirm');
Route::post('/forgot-password-link-reset', 'Api\AuthController@forgotPasswordLinkReset');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/logout', 'Api\AuthController@logout');
    Route::post('/refresh', 'Api\AuthController@refresh');
    Route::get('/user-profile', 'Api\AuthController@userProfile');
    Route::post('/upload-avatar', 'Api\AuthController@uploadAvatar');
    Route::patch('close-account', 'Api\AuthController@closeAccount');
    Route::patch('display-profile/toggle', 'Api\AuthController@toggleDisplayProfile');
    Route::patch('change-password', 'Api\AuthController@changePassword');
});
Route::post('/verify_username', 'Api\AuthController@verifyUsername');


/*
------------------------------------------------------------------------
|    CONTACT'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(['middleware' => 'auth:api', "prefix" => "contact"], function () {
    Route::get('/{contact}/registration-information', [ContactController::class, 'fetchARegisteredContactInformation']);
    Route::get('google', [GoogleContact::class, 'fetchContact']);
    Route::post('import', [ContactController::class, 'importContact']);
    Route::get('my-contacts', [ContactController::class, 'fetchAUserContact']);
    Route::get('search', [ContactController::class, 'searchContact']);
    Route::get('search-users', [FriendController::class, 'searchUsers']);
});

/*
------------------------------------------------------------------------
|    PROFILE'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "profile"], function () {
    Route::group(['prefix' => 'bank-details'], function () {
        Route::get('/', [BankDetailsController::class, 'fetchBankDetail']);
        Route::post('/', [BankDetailsController::class, 'computeBankDetails']);
    });
    Route::get('/birthd', [ProfileController::class, 'birthd']);
    Route::get('/', [ProfileController::class, 'getCurrentUserProfile']);
    Route::put('/', [ProfileController::class, 'updateUserProfile']);
    Route::put('/address', [ProfileController::class, 'updateUserAddress']);
    Route::put('/close-account', [ProfileController::class, 'closeAccount']);
    Route::get('/{user}', [ProfileController::class, 'getOtherUserProfile']);



    Route::group(['prefix' => 'wallets'], function () {
        Route::post('/fund', [WalletController::class, 'fundMyWallet']);
        Route::post('/withdraw', [WalletController::class, 'withdrawFromMyWallet']);
        Route::get('/detail', [WalletController::class, 'walletBalance']);
    });
});

/*
------------------------------------------------------------------------
|    TRANSACTION'S / PAYMENT'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["prefix" => "payments"], function () {
    Route::post('/webhook', [Flutterwave::class, 'updateSuccessfulTransaction']);
    Route::get('/banks', [Flutterwave::class, 'getBanks']);
    Route::get('/countries', [Flutterwave::class, 'getCountries']);
    Route::get('/history', [Flutterwave::class, 'fetchTransactionHistory'])->middleware('auth:api');
    Route::post('/accounts/verify', [Flutterwave::class, 'resolveAccountName']);
});

/*
------------------------------------------------------------------------
|    INVENTORIES ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "inventories"], function () {
    Route::group(["prefix" => "local"], function () {
        Route::get('/', [LocalInventoryController::class, 'index']);
        Route::get('/{inventory}', [LocalInventoryController::class, 'show']);
        Route::post('/{inventory}/review', [ReviewControllerForCelebrant::class, 'rateAnInventory']);
        Route::get('/get-vendor-products/{vendor_id}', [LocalInventoryController::class,'getVendorProduct']);


    });

    Route::group(["prefix" => "global"], function () {
        Route::get('/vendors', [GlobalInventoryController::class, 'vendor']);
        Route::post('/{inventory}/review', [ReviewControllerForCelebrant::class, 'rateAnInventory']);
    });
    Route::get('/{inventory}/review', [ReviewControllerForCelebrant::class, 'getAnInventoryReview']);
});

/*
------------------------------------------------------------------------
|    FRIEND'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "friends"], function () {
    Route::post('/', [FriendController::class, 'sendFriendRequest']);
    Route::get('/', [FriendController::class, 'myFriends']);
    Route::put('/action', [FriendController::class, 'requestAction']);
});

/*
------------------------------------------------------------------------
|    REPORT'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "reports"], function () {
    Route::post('/', [ReportController::class, 'createReport']);
    Route::get('/', [ReportController::class, 'getUserReports']);
});

/*
------------------------------------------------------------------------
|    FEEDBACK'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "feedbacks"], function () {
    Route::post('/', [FeedbackController::class, 'createFeedbacks']);
    Route::get('/', [FeedbackController::class, 'getUserFeedbacks']);
});
/*
------------------------------------------------------------------------
|    FEED'S AND STATUS' ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "statuses"], function () {
    Route::post('/', [StatusController::class, 'uploadStatus']);
    Route::get('/', [StatusController::class, 'fetchUserStatuses']);
    Route::post('/{status}/viewers', [StatusController::class, 'recordViewers']);
    Route::get('/{status}/viewers', [StatusController::class, 'viewers']);
    Route::get('/all', [StatusController::class, 'fetchAllMyConnectionStatus']);
    Route::get('/collage', [StatusController::class, 'collageDetails']);
    Route::get('/{user}', [StatusController::class, 'fetchAUserStatus']);
    Route::delete('/{status}', [StatusController::class, 'deleteStatus']);
});

/*
------------------------------------------------------------------------
|    CHAT'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "chats"], function () {
    Route::post('{friend}', [ChatController::class, 'sendMessage']);
    Route::get('{friend}', [ChatController::class, 'aConnectionChat']);
    Route::get('users/{friend}', [ChatController::class, 'userChatId']);
});

/*
------------------------------------------------------------------------
|    WISHLIST'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "wishlists"], function () {
    Route::get('/', [WishListController::class, 'index']);
    Route::post('/', [WishListController::class, 'store']);
    Route::delete('/{wishlist}', [WishListController::class, 'delete']);
    Route::post('/set-priority', [WishListController::class, 'setWishlistPriority']);
    Route::get('/{wishlist}/contributors', [WishListController::class, 'fetchWishListItemContributors']);
    Route::get('/{wishlist}/contributorsdelete', [WishListController::class, 'DeleteWishListItemContributors']);

    Route::post('/{wishlist}/contribute', [WishListController::class, 'contribute']);
    Route::get('/contributors', [WishListController::class, 'fecthAllContributors']);


});

/*
------------------------------------------------------------------------
|    VENDOR'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(['prefix' => 'vendors'], function () {
    Route::group(['middleware' => 'auth:vendor'], function () {
        Route::get('/', [AuthController::class, 'fetchAuthenticatedVendorProfile']);
        Route::get('/categories', [InventoryController::class, 'fetchCategories']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/refresh-token', [AuthController::class, 'refresh']);
        Route::put('/update', [AuthController::class, 'updateVendorProfile']);

        Route::group(['prefix' => 'inventories'], function () {
            Route::post('/', [InventoryController::class, 'store']);
            Route::get('/', [InventoryController::class, 'index']);
            Route::get('/all', [InventoryController::class, 'allInventories']);
            Route::get('/get-utilized-categories', [InventoryController::class, 'getCategories']);
            Route::get('/get-vendor-analysis', [InventoryController::class, 'getVendorAnalysis']);
            Route::post('/inventory-multiple-actions', [InventoryController::class, 'inventoryMultipleFunction']);


            Route::get('/{inventory}', [InventoryController::class, 'show']);
            Route::put('/{inventory}', [InventoryController::class, 'update']);
            Route::delete('/{inventory}', [InventoryController::class, 'delete']);
            Route::get('/{inventory}/reviews', [ReviewController::class, 'getAnInventoryReview']);
        });

        Route::group(['prefix' => 'deliveries'], function () {
            Route::put('/{wishlist}', [DeliveryStatusController::class, 'setStatus']);
            Route::get('/{wishlist}', [DeliveryStatusController::class, 'getDeliveryStatus']);
        });
    });

    Route::post('/sign-up', [AuthController::class, 'signUp']);
    Route::post('/sign-in', [AuthController::class, 'signIn']);
    Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);
    Route::put('/verify', [AuthController::class, 'verifyEmail']);
    Route::post('/verify/resend-token', [AuthController::class, 'resendVerificationToken']);
});

/*
------------------------------------------------------------------------
|    NOTIFICATION ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(["middleware" => "auth:api", "prefix" => "notifications"], function () {
    Route::get('/', [NotificationController::class, 'index']);
    Route::post('/markall-as-read', [NotificationController::class, 'readAllNotification']);
    Route::post('/{id}', [NotificationController::class, 'markNotifcationAsRead']);
    });


/*
------------------------------------------------------------------------
|    ADMIN'S ENDPOINTS
|-----------------------------------------------------------------------
 */
Route::group(['prefix' => 'admins'], function () {
    Route::group(['middleware' => 'auth:admins'], function () {
        Route::get('/', [AdminAuthController::class, 'fetchAuthenticatedadminProfile']);
        Route::get('/categories', [InventoryController::class, 'fetchCategories']);
        Route::get('/get-vendors', [VendorAccountController::class, 'fecthVendors']);
        Route::post('/logout', [AdminAuthController::class, 'logout']);
        Route::post('/refresh-token', [AdminAuthController::class, 'refresh']);
    });

    Route::post('/create-account', [AdminAuthController::class, 'createAccount']);
    Route::post('/sign-in', [AdminAuthController::class, 'signIn']);
    Route::post('/forgot-password', [AdminAuthController::class, 'forgotPassword']);
    Route::post('/reset-password', [AdminAuthController::class, 'resetPassword']);
    Route::post('/verify', [AdminAuthController::class, 'verifyEmail']);
    Route::post('/verify/resend-token', [AdminAuthController::class, 'resendVerificationToken']);


    Route::group(['middleware' => 'auth:admins', 'prefix' => 'categories'], function () {
        Route::post('/', [CategoryController::class, 'create']);
        Route::put('/{category}', [CategoryController::class, 'update']);
        Route::get('/{category}', [CategoryController::class, 'show']);
        Route::delete('/{category}', [CategoryController::class, 'delete']);
    });

    Route::put('/vendors/toggle', [VendorAccountController::class, 'activateOrDeactivateAccount']);
    Route::put('/celebrants/toggle', [CelebrantAccountController::class, 'activateOrDeactivateAccount']);

    Route::group(["middleware" => "auth:admins", "prefix" => "global-vendors"], function () {
        Route::post('/', [AdminGlobalInventoryController::class, 'create']);
        Route::get('/', [AdminGlobalInventoryController::class, 'index']);
        Route::delete('/{globalVendor}', [AdminGlobalInventoryController::class, 'delete']);
    });
});
Route::post('/update', [ProfileController::class, 'updateUser']);
Route::post('/update2', [ProfileController::class, 'updateUser2']);



Broadcast::routes(['middleware' => 'auth:api']);
