<?php

use App\Http\Controllers\Api\Admin\AuthController as AdminAuthController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('payment');
});
Route::get('/home', 'Web\HomeController@index')->name("login");
Route::get('/payment-successful', [AdminAuthController::class, 'paymentSuccess']);
