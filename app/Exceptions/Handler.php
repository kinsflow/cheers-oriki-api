<?php

namespace App\Exceptions;

use App\Traits\SendApiResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;


class Handler extends ExceptionHandler
{
    use SendApiResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (Throwable $e, $request) {
            if ($request->is('api/*')) {
                if ($e instanceof NotFoundHttpException) {
                    return $this->failureResponse("Route Not Found", Response::HTTP_NOT_FOUND);
                }

                if ($e instanceof AccessDeniedHttpException) {
                    return $this->failureResponse("Unauthorized", Response::HTTP_FORBIDDEN);
                }
            }
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof TokenInvalidException) {
            return response()->json(['status' => 'failed', 'message' => 'Invalid token'], 401);
        }
        if ($e instanceof TokenExpiredException) {
            return response()->json(['status' => 'failed', 'message' => 'Token has expired'], 401);
        }
        if ($e instanceof JWTException) {
            return response()->json(['status' => 'failed', 'message' => 'Token not parsed '], 401);
        }
        return parent::render($request, $e);
    }
}
