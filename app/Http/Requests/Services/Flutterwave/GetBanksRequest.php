<?php

namespace App\Http\Requests\Services\Flutterwave;

use App\Enums\FlutterwaveCountries;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetBanksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_abbr' => ['required', 'string', Rule::in([
                FlutterwaveCountries::NIGERIA,
                FlutterwaveCountries::GHANA,
                FlutterwaveCountries::KENYA,
                FlutterwaveCountries::UGANDA,
                FlutterwaveCountries::SOUTH_AFRICA,
                FlutterwaveCountries::TANZANIA,
            ])]
        ];
    }
}
