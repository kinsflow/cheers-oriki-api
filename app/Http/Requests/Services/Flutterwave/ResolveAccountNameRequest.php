<?php

namespace App\Http\Requests\Services\Flutterwave;

use Illuminate\Foundation\Http\FormRequest;

class ResolveAccountNameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_bank' => ['required', 'string'],
            'account_number' => ['required', 'string']
        ];
    }
}
