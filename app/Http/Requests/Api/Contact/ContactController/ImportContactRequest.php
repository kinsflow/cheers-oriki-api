<?php

namespace App\Http\Requests\Api\Contact\ContactController;

use Illuminate\Foundation\Http\FormRequest;

class ImportContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.name' => 'required|string',
            '*.email' => 'nullable|string',
            '*.phone' => 'required|string',
            '*.birthday' => 'nullable|date'
        ];
    }
}
