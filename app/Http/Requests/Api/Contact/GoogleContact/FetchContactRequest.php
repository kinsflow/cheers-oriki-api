<?php

namespace App\Http\Requests\Api\Contact\GoogleContact;

use Illuminate\Foundation\Http\FormRequest;

class FetchContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required|string',
            'next_page_token' => 'sometimes|string'
        ];
    }
}
