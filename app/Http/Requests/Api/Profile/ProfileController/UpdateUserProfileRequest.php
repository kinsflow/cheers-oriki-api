<?php

namespace App\Http\Requests\Api\Profile\ProfileController;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone' => ['required', 'string', Rule::unique('users')->ignore(Auth::id())],
            'username' => ['required', Rule::unique('users')->ignore(Auth::id())],
            'date_of_birth' => ['required', 'date', 'date_format:Y-m-d'],
            'email' => ['required', 'email:dns', Rule::unique('users')->ignore(Auth::id())],
            'avatar' => ['sometimes', 'image', 'mimes:jpg,png,jpeg,svg'],
            'device_token' => ['nullable']
        ];
    }
}
