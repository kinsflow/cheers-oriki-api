<?php

namespace App\Http\Requests\Api\Profile\WalletController;

use App\Enums\FlutterwaveCurrencies;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WithdrawFromMyWalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_bank' => ['required', 'string'],
            'account_number' => ['required', 'string'],
            'amount' => ['required', 'integer'],
            'transaction_date' => ['required', 'date'],
            'password' => ['required'],
            'currency' => ['required', Rule::in([
                FlutterwaveCurrencies::NIGERIA,
                FlutterwaveCurrencies::GHANA,
                FlutterwaveCurrencies::KENYA,
                FlutterwaveCurrencies::UGANDA,
                FlutterwaveCurrencies::SOUTH_AFRICA,
                FlutterwaveCurrencies::TANZANIA,
            ])]
        ];
    }
}
