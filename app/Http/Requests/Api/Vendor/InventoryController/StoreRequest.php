<?php

namespace App\Http\Requests\Api\Vendor\InventoryController;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'category_id' => ['required', 'exists:categories,id'],
            'gallery' => ['nullable', 'array'],
            'gallery.*' => ['mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime,image/png,image/jpeg'] ,
            'price' => ['required','between:0,9999999.99']
        ];
    }
}
