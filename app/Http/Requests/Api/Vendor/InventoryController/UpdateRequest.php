<?php

namespace App\Http\Requests\Api\Vendor\InventoryController;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['sometimes', 'string'],
            'description' => ['sometimes', 'string'],
            'vendor_id'=> ['sometimes', 'exists:vendors,id'],
            'category_id' => ['sometimes', 'exists:categories,id'],
            'gallery' => ['sometimes', 'array'],
            'gallery.*' => ['mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime,image/png,image/jpeg'] ,
            'price' => ['sometimes','between:0,9999999.99']
        ];
    }
}
