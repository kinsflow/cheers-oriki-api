<?php

namespace App\Http\Requests\Api\Vendor\WishListController;

use Illuminate\Foundation\Http\FormRequest;

class ContributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wishlist_id' => ['required', 'exists:wishlists,id'],
            'amount' => ['required', 'between:0,9999999.99']
        ];
    }
}
