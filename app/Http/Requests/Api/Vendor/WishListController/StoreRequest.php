<?php

namespace App\Http\Requests\Api\Vendor\WishListController;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model_id' => ['required_if:model,inventory', 'integer'],
            'title' => ['required_if:model,global_inventory', 'string'],
            'image' => ['required_if:model,global_inventory', 'mimetypes:image/png,image/jpeg'],
            'description' => ['sometimes', 'string'],
            'price' => ['required_if:model,global_inventory', 'between:0,9999999.99'],
            'model' => ['required', 'in:inventory,global_inventory'],
            'quantity' => ['nullable', 'integer'],
        ];
    }
}
