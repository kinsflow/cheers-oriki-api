<?php

namespace App\Http\Requests\Api\Vendor\AuthController;

use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:vendors,email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'vendor_name' => 'required|string',
            'phone' => 'required|string|unique:vendors,phone',
            'password' => 'required|string|confirmed|min:8',
            // 'username' => 'required|string|unique:vendors,username',
            'profile_picture' => ['image', 'mimetypes:image/png,image/jpeg,image/jpg']
        ];
    }
}
