<?php

namespace App\Http\Requests\Api\Chat\ChatController;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_text' => ['required', 'bool'],
            'text' => ['required_if:is_text,1,true'],
            'chat_media' => ['required_if:is_text,0,false', 'mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime,image/png,image/jpeg']
        ];
    }
}
