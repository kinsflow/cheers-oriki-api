<?php

namespace App\Http\Requests\Api\Inventory\ReviewController;

use Illuminate\Foundation\Http\FormRequest;

class RateAnInventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => ['nullable', 'string'],
            'rating' => ['required', 'string', 'min:1', 'max:5']
        ];
    }
}
