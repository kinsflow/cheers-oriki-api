<?php

namespace App\Http\Requests\Api\Home\StatusController;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UploadStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $current_birthday = Auth::user()->date_of_birth;
//
//        $threeDaysBefore = Carbon::parse($current_birthday)->subDays(3)->format('md');
//        $threeDaysAfter = Carbon::parse($current_birthday)->addDays(3)->format('d M');
//
//        $todaysDate = Carbon::today()->format('d M');
//        if ($todaysDate <= $threeDaysBefore) {
//            dd('the data is lesser', $todaysDate, $threeDaysBefore);
//        }
//
//        if ($todaysDate >= $threeDaysAfter) {
//            dd('the date is older');
//        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'is_text' => ['required', 'bool'],
            'text' => ['required_if:is_text,1,true'],
            'font' => ['required_if:is_text,1,true'],
            'color' => ['required_if:is_text,1,true'],
            'status_media' => ['required_if:is_text,0,false', 'mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime,image/png,image/jpeg']
        ];
    }
}
