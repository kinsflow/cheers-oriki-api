<?php

namespace App\Http\Requests\Api\Friends\FriendController;

use App\Enums\FriendStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RequestActionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action' => ['required', Rule::in([
                FriendStatus::CONFIRMED,
                FriendStatus::BLOCKED,
                FriendStatus::DECLINED,
                FriendStatus::UNBLOCKED
            ])],
            'friend_list_id' => ['required', 'exists:friends,id']
        ];
    }
}
