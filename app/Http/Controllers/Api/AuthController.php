<?php

namespace App\Http\Controllers\Api;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Traits\SendApiResponse;
use App\Traits\SendFCMNotification;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tzsk\Otp\Facades\Otp;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Repositories\UploadImageRepository;
use App\Repositories\RepositoryInterfaces\UploadImageInterface;
use App\Rules\UsernameOrPhoneRule;
use App\Mail\CloudHostingMail;
use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Jobs\SendEmail;
use App\Http\Controllers\Api\Contact\ContactController;
use App\Models\Feedback;
use App\Models\Friend;
use Illuminate\Http\Response;

class AuthController extends Controller
{

    use SendApiResponse;
    use SendFCMNotification;

    private $uploadImageRepository;


    public function __construct(UploadImageRepository $uploadImageRepository)
    {
        $this->uploadImageRepository = $uploadImageRepository;
    }

    /**
     * Get one time password for unique phone number.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function generate(Request $request)
    {

        // $rule = ['phone' => 'required|regex:/[0-9]{11}/|unique:users'];
        // $messages = ['phone.required' => "You can't leave phone number field empty",
        //               'phone.regex' => 'The field must be 11 digit numeric character',
        //               'phone.unique' => 'User with this phone number already exists'
        //             ];
        $rule = ['email' => 'required|email|unique:users'];
        $messages = ['email.required' => "You can't leave email field empty",
            'email.email' => 'Email field must be a valid email format',
            'email.unique' => 'User with this email already exists'
        ];
        $validator = Validator::make($request->all(), $rule, $messages);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()
            ], 422);
        };
        // generate otp
        $otp = Otp::digits(5)->expiry(10)->generate($request->email);
        $data = array("email" => $request->email, "otp" => $otp, "template" => "otp", "subject" => "One Time Password");
        //queue job
        SendEmail::dispatch($data)->delay(now()->addSeconds(5));
        return response()->json(['status' => 'success', 'message' => 'otp has been sent successfully', 'data' => $otp]);

        // /*  Send SMS */
        // $one_time_password = (int)$otp;
        // $data = array('recipients' => $request->phone, 'message' => 'Welcome to Cheers. OTP: ' . $one_time_password .' It expires after 10 minutes. For any help please contact us on ' . env("CHEERS_CONTACT_NUMBER"));
        // if (sendOtpSMS($data)) {
        //     return response()->json(['status' => 'success', 'message' => 'otp sent successfully','data' => ["otp" => $otp, "phone" => $request->phone]], 200);
        // }
        // else {
        //     return response()->json(['status' => 'failed', 'message' => 'unabled to send otp at this time'], 409);
        // }

    }


    /**
     * verify one-time-password.
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function verify(Request $request)
    {
        // $rule = ['phone' => 'required|regex:/[0-9]{11}/|unique:users',
        //           'otp' => 'required|regex:/[0-9]{5}/'
        //         ];
        // $messages = [
        //     'phone.required' => "You can't leave phone number field empty",
        //     'phone.regex' => 'The field must be 11 digit long',
        //     'otp.required' => "You can't leave otp field empty",
        //     'otp,regex' => "The field must be 5 digit long"
        // ];
        $rule = ['email' => 'required|email|unique:users',
            'otp' => 'required|regex:/[0-9]{4}/'
        ];
        $messages = [
            'email.required' => "You can't leave email field empty",
            'email.email' => 'Email field must be a valid email format',
            'email.unique' => 'User with this email already exists',
            'otp.required' => "You can't leave otp field empty",
            'otp.regex' => "The field must be 5 digit long"
        ];
        $validator = Validator::make($request->all(), $rule, $messages);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()
            ], 422);
        };

        $verify = Otp::digits(5)->expiry(10)->check($request->otp, $request->email);
        // generate access token
        $access_token = Otp::digits(12)->expiry(10)->generate($request->email);
        if ($verify) {
            return response()->json(['status' => 'success', 'access_token' => $access_token, 'message' => 'one-time-password has been verify successfully'], 200);
        } else {
            return response()->json(['status' => 'failed', 'message' => 'invalid one-time-password'], 409);
        }

    }


    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
           $validator = Validator::make($request->all(), [
                'first_name' => 'required|string|max:20',
                'last_name' => 'required|string|max:20',
                'email' => 'required|email|unique:users',
                'username' => 'required|string|max:20|regex:/[^0-9]+.*/|unique:users',
                'phone' => 'required|regex:/[0-9]{11}/|unique:users',
                // 'date_of_birth' => 'required|date|before_or_equal:' . Carbon::now()->subYear(18)->format('d-m-Y'),
                'date_of_birth' => 'required|date',
                'avatar' => 'sometimes|image|mimes:jpg,png,jpeg,svg',
                'password' => 'required|string|confirmed|min:6',
                'invite_token' => 'sometimes|string',
                'device_token' => 'nullable|string'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($errors->all() as $message)
                {
                    return response()->json([
                       'status' => 'failed',
                       'message' => $message
                    ], 422);
                }
            }
            if ($request->hasHeader("X-Access-Token")) {
                $access_token = $request->header("X-Access-Token");
                $verify = Otp::digits(12)->expiry(10)->check($access_token, $request->email);
                if (!$verify) {
                    return response()->json(['status' => 'failed', 'message' => 'invalid/expired access_token'], 409);
                }
            } else {
                return response()->json(['status' => 'failed', 'message' => 'access token missing in request header'], 409);
            }

            $validated_data = $validator->validated();
            unset($validated_data['avatar']);
            $user = User::create(array_merge(
                $validated_data,
                ['password' => bcrypt($request->password),
                    'email_verified_at' => Carbon::now(),
                    'remember_token' => bcrypt($request->header("X-Access-Token"))]
            ));
            if ($request->invite_token) (new ContactController())->manageRegisteringContacts($request, $user);

            // modify user status on contact list
            Contact::where('phone', $request->phone)->orWhere('email', $request->email)->update([
                'is_registered' => true
            ]);

            if ($user->exists()) {
                if ($request->avatar) {
                    $user->addMediaFromRequest('avatar')->toMediaCollection(MediaCollections::PROFILEPICTURE);
                }

                $crendetials = ['email' => $user->email, 'password' => $request->password];
                if (!$token = auth()->attempt($crendetials, true)) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'Invalid login credentials'
                    ], 401);
                } else {
                    DB::commit();
                    $data = array("email" => $user->email, "password" => $request->password, "first_name" => ucfirst($user->first_name), "template" => "register", "subject" => "Welcome to Cheers", "username" => $user->username);
                    //queue job
                    SendEmail::dispatch($data)->delay(now()->addSeconds(5));
                    return response()->json([
                        'status' => 'success',
                        'message' => 'User successfully registered',
                        'token' => $token,
                        'expires_in' => (string)auth()->factory()->getTTL() . ' minutes',
                        'user' => $user
                    ], 200);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage()
            ], 500);

        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email_or_username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()
                ],
                422
            );
        }
        $fieldType = filter_var($request->email_or_username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if ($token = auth()->attempt(array($fieldType => $request->email_or_username, 'password' => $request->password))) {
            if (auth()->user()->is_closed){
                return $this->failureResponse("This account has been closed, reach out to admin to get your account reactivated", 403);
            }

            $this->sendSingleDevice('Signed In','You just signed into your account', 'SIGN_IN', \auth()->id());

            return $this->createNewToken($token);
        } else {
            return response()->json(['status' => 'failed', 'message' => 'Invalid login credentials'], 401);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['status' => 'success', 'message' => 'User successfully signed out'], 200);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        try {
            $user = auth()->user();
            if ($user) {
                return response()->json(["status" => "success", "data" => auth()->user()], 200);
            } else {
                return response()->json(['status' => 'failed', 'message' => 'Invalid user'], 422);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage()
            ], 400);
        }


    }


    public function forgotPasswordLink(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'phone_or_username' => ['required', new UsernameOrPhoneRule]
        // ]);
        // if ($validator->fails()) {
        //     return response()->json(
        //         ['status' => 'failed', 'message' => $validator->errors()],
        //         422
        //     );
        // }
        // $fieldType = is_numeric($request->phone_or_username) ? 'phone' : 'username';
        // $user = User::where($fieldType, $request->phone_or_username)->first();
        $rule = ['email' => 'required|email'];
        $messages = ['email.required' => "You can't leave email field empty",
            'email.email' => 'Email field must be a valid email format'
        ];
        $validator = Validator::make($request->all(), $rule, $messages);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()
            ], 422);
        };

        //Check if email exists in the database
        $checkIfEmailExist = User::where("email", $request->email)->exists();
        if(!$checkIfEmailExist)
        {
            return response()->json([
                'status' => 'failed',
                'message' => 'User email does not exist'

            ], 404);
        }

        $user = User::where("email", $request->email)->first();

        $otp = Otp::digits(5)->expiry(10)->generate($user->email);
        $data = array("email" => $user->email, "otp" => $otp, "first_name" => ucfirst($user->first_name), "template" => "forgot-password", "subject" => "Forgot Password Link");
        //queue job
        SendEmail::dispatch($data)->delay(now()->addSeconds(5));
        return response()->json(['status' => 'success', 'message' => 'forgot password link sent successfully', 'data' => $otp]);

    }


    public function forgotPasswordLinkConfirm(Request $request)
    {
        $rule = [
            'email' => 'required|email|exists:users,email',
            'otp' => 'required|regex:/[0-9]{5}/'
        ];
        $messages = [
            'email.required' => "You can't leave email field field empty",
            'email.email' => 'The field must be a valid email address',
            'email.exits' => 'The field must be a valid email address of an existing user',
            'otp.required' => "You can't leave otp field empty",
            'otp,regex' => "The field must be 5 digit long"
        ];
        $validator = Validator::make($request->all(), $rule, $messages);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()
            ], 422);
        };
        $verify = Otp::digits(5)->expiry(10)->check($request->otp, $request->email);
        if ($verify) {
            // generate access token
            $access_token = Otp::digits(12)->expiry(10)->generate($request->email);
            return response()->json(['status' => 'success', 'message' => 'one-time-password verified successfully', 'access_token' => $access_token], 200);
        } else {
            return response()->json(['status' => 'failed', 'message' => 'invalid one-time-password'], 400);
        }
    }


    public function forgotPasswordLinkReset(Request $request)
    {
        DB::beginTransaction();
        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|email|exists:users,email',
        //     'password' => 'required|string|confirmed|min:6',
        // ]);
        $rule = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string|confirmed|min:6'
        ];
        $messages = [
            'email.required' => "You can't leave email field field empty",
            'email.email' => 'The field must be a valid email address',
            'email.exists' => 'The field must be a valid email address of an existing user',
            'password.required' => 'The password field cannot be left empty',
            'password.confirmed' => 'The password field should match',
        ];
        $validator = Validator::make($request->all(), $rule, $messages);
        $user = User::where("email", $request->email)->first();
        if ($user->exists) {
            if ($validator->fails()) {
                return response()->json(
                    ['status' => 'failed', 'message' => $validator->errors()],
                    400
                );
            }
            if ($request->hasHeader("X-Access-Token")) {
                $access_token = $request->header("X-Access-Token");
                $verify = Otp::digits(12)->expiry(10)->check($access_token, $user->email);
                if (!$verify) {
                    return response()->json(['status' => 'failed', 'message' => 'invalid/expired access_token'], 409);
                }
            } else {
                return response()->json(['status' => 'failed', 'message' => 'access token missing in request header'], 409);
            }
            $user->update(["password" => Hash::make($request->password)]);
            DB::commit();
            if ($token = auth()->attempt(array("email" => $request->email, 'password' => $request->password))) {
                return $this->createNewToken($token);
            } else {
                DB::rollBack();
                return response()->json(['status' => 'failed', 'message' => 'Invalid login credentials'], 422);
            }
        } else {
            return response()->json(['status' => 'failed', 'message' => 'Invalid user'], 422);
        }


    }


    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ], 200);
    }


    public function uploadAvatar(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'avatar' => 'image|mimes:jpg,jpeg,bmp,svg,png|max:5000'
            ]);
            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => 'failed',
                        'message' => 'The given data was invalid.',
                        'errors' => $validator->errors()
                    ],
                    422
                );
            }
            if (is_null(Auth()->user()->avatar)) {
                $user = Auth()->user()->update(['avatar' => $this->uploadImageRepository->upload($request) ?: null]);
                if ($user) {
                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'successfully'], 200);
                } else {
                    DB::rollBack();
                    return response()->json(['status' => 'failed', 'message' => 'unabled to upload image at this time'], 422);
                }
            } else {
                Storage::disk('s3')->delete(auth()->user()->avatar);
                $user = Auth()->user()->update(['avatar' => $this->uploadImageRepository->upload($request) ?: null]);
                if ($user) {
                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'successfully'], 200);
                } else {
                    DB::rollBack();
                    return response()->json(['status' => 'failed', 'message' => 'unabled to upload image at this time'], 422);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage()
            ], 422);
        }
    }


    public function verifyUsername(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:20|regex:/[^0-9]+.*/',
            'first_name' => 'required|string|max:20',
            'last_name' => 'string|max:20'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()
            ], 422);
        }
        if (User::where("username", $request->username)->exists()) {
            $suggestion_box = [];
            $suggestion_complete = True;
            while ($suggestion_complete) {
                $array = array();
                $array[0] = strtolower($request->first_name) . (strtolower($request->last_name) ?? '') . '@' . rand(1, 9);
                $array[1] = ucwords($request->first_name) . (strtolower(@$request->last_name[0]) ?? '') . '@' . rand(10, 90);
                $array[2] = strtolower($request->first_name) . (strtolower(@$request->last_name) ?? '') . '@' . rand(100, 900);
                $suggestion = $array[rand(0, (count($array) - 2))];
                $result = User::where("username", $suggestion)->first();
                if (!$result) {
                    if (count($suggestion_box) <= 2) {
                        array_push($suggestion_box, $suggestion);
                    } else {
                        $suggestion_complete = False;
                    }
                }
            }
            return response()->json(['status' => 'success', 'validation_result' => false, 'suggestion' => $suggestion_box], 200);
        } else {
            return response()->json(['status' => 'success', 'validation_result' => true, 'suggestion' => null], 200);
        };
    }

    /**
     * Close a user Account.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function closeAccount(Request $request)
    {
        $request->validate([
            'feedback' => 'required|string'
        ]);

        Feedback::create([
            'title' => 'Closed Account',
            'body' => $request->feedback,
        ]);

        // delete all friend related connections
        Friend::query()
        ->where('first_user', auth()->id())
        ->orWhere('second_user', auth()->id())
        ->delete();

        $user = \auth()->user();

        \auth()->invalidate(false);

        $user->forceDelete();

        return $this->successResponse('','Account Closed Successfully', Response::HTTP_NO_CONTENT);
    }

    /**
     * Toggle/Change the display status of certain user details.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleDisplayProfile()
    {
        $user = \auth()->user();
        $user->hide_profile = !$user->hide_profile;

        $user->save();

        return $this->successResponse('','Display profile status toggled Successfully');
    }

    /**
     * Change user password.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $userInfo = \auth()->user();

        $request->validate([
            'old_password' => 'required|string',
            'new_password' => 'required|string|confirmed|min:6'
        ]);

        $checkPassword = Hash::check($request->old_password, $userInfo->password);

        if (!$checkPassword){
            return $this->failureResponse('The old password entered is not correct');
        }

        $userInfo->password = bcrypt($request->new_password);

        $userInfo->save();

        return $this->successResponse('', 'Password change was successful');
    }
}
