<?php

namespace App\Http\Controllers\Api\Chat;

use App\Enums\FriendStatus;
use App\Enums\MediaCollections;
use App\Events\ChatMessageSent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Chat\ChatController\SendMessageRequest;
use App\Models\Chat;
use App\Models\Friend;
use App\Traits\SendApiResponse;
use App\Traits\SendFCMNotification;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * This method returns all the chat of a particular friend
     * connection.
     *
     * @param Friend $friend
     * @return \Illuminate\Http\JsonResponse
     */
    public function aConnectionChat(Friend $friend, Request $request)
    {
        $chats = $friend->chats()->with(['sender'])->paginate($request->per_page);

        return $this->successResponse($chats, 'Friend Connection Chats Returned Successfully');
    }

    /**
     * Send a message to a friend.
     *
     * @param Friend $friend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function sendMessage(Friend $friend, SendMessageRequest $request)
    {
        throw_unless(
            $friend->where('first_user', auth()->id())->orWhere('second_user', auth()->id())->whereStatus(FriendStatus::CONFIRMED)->first(),
            'You Cannot Send message to this user. it is possible you don\'t have a confirmed friend connection with this person');

       $chat = $friend->chats()->create([
           'sender_id' => auth()->id(),
           'is_text' => $request->is_text,
           'text' => $request->text,
           'recipient_id' => $friend->second_user,
        ]);

        if ($request->chat_media && !$request->is_text) {
            $chat->addMediaFromRequest('chat_media')->toMediaCollection(MediaCollections::CHAT);
        }

        event(new ChatMessageSent($chat));

        $notificationReceiverId =  $chat->friendConnection->first_user == auth()->id() ? $chat->friendConnection->second_user : $chat->friendConnection->first_user;

        $this->sendSingleDevice('new message', 'you have a new message in your chat', 'CHAT_MESSAGE', $notificationReceiverId);

        return $this->successResponse($chat, 'Chat Message Sent successfully');
    }


    public function userChatId(Request $request)
    {
        $chats = Chat::where('id', auth()->id())
        ->select('id as chatId', 'sender_id', 'recipient_id')
        ->with([
        'sender' => function($query)
        {
        $query->select('id', 'first_name', 'last_name');
        },
        'recipient' => function($query)
        {
        $query->select('id', 'first_name', 'last_name');
        },
        ])->paginate($request->per_page);

        return $this->successResponse($chats, 'User Chats Connection Returned Successfully');




    }
}
