<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\AuthController\CreateAccountRequest;
use App\Http\Requests\Api\Admin\AuthController\ForgotPasswordRequest;
use App\Http\Requests\Api\Admin\AuthController\SignInRequest;
use App\Http\Requests\Api\Admin\AuthController\ResendVerificationTokenRequest;
use App\Http\Requests\Api\Admin\AuthController\ResetPasswordRequest;
use App\Http\Requests\Api\Admin\AuthController\VerifyRequest;
use App\Jobs\SendEmail;
use App\Models\Admin;
use App\Traits\SendApiResponse;
use Exception;
use Illuminate\Http\Response;
use Tzsk\Otp\Facades\Otp;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    use SendApiResponse;

    /**
     * Admin Create Another Admin Account.
     *
     *
     * @param CreateAccountRequest $request
     * @return void
     */
    public function createAccount(CreateAccountRequest $request)
    {
        try{$data = $request->validate([
            'email' => 'required|email|unique:vendors,email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string|unique:vendors,phone',
            'password' => 'required|string|confirmed|min:8'
        ]);}
        catch(Exception $e){ return $this->failureResponse($e->getMessage());}
       $admin = Admin::create([
           'first_name' => $request->first_name,
           'last_name' => $request->last_name,
           'phone' => $request->phone,
           'email' => $request->email,
           'password' => bcrypt($request->password)
        ]);


        $admin->sendEmailVerificationNotification();

        return $this->successResponse($admin, 'Account Creation Succesfull. A verification code has being sent to user email, user should use the code to verify your email');
    }

    /**
     * Sign In as An Admin.
     *
     * @param SignInRequest $request
     * @return void
     */
    public function signIn(SignInRequest $request)
    {
        $admin = Admin::whereEmail($request->email)->first();

        // abort_if(!$admin, Response::HTTP_NOT_ACCEPTABLE, 'E');
        if(!$admin)
        {
        return response()->json([
            'message' => 'Email does not exists in our record',
            'error' => true
        ], Response::HTTP_NOT_ACCEPTABLE);
    }
        $isEmailVerified = $admin->email_verified_at;

        !($isEmailVerified) ? $admin->sendEmailVerificationNotification(): null;

        if(!($isEmailVerified)){
            return response()->json([
               'message' => 'Account has not being verified check your email for verification code',
                'error' => true
            ], Response::HTTP_FORBIDDEN);
        }

        auth()->shouldUse('admin');

        $token = auth()->attempt([
           'email' => $request->email,
           'password' => $request->password
        ]);

        if (!$token){
            return $this->failureResponse('Incorrect Email Or Password', Response::HTTP_UNAUTHORIZED);
        }

        return $this->successResponse(['token' => $token], 'Signed In Successfully');
    }

    /**
     * Verify Email Of A Just Created User.
     *
     * @param VerifyRequest $request
     * @return void
     */
    public function verifyEmail(VerifyRequest $request)
    {
        $verify = Otp::digits(6)->expiry(10)->check($request->token, $request->email);

        if (!$verify){
            return $this->failureResponse('invalid/expired access_token', Response::HTTP_FORBIDDEN);
        }

        $admin = Admin::whereEmail($request->email)->first();

        $admin->markEmailAsVerified();

        return $this->successResponse(['admin' => $admin], 'Email Verified Successfully');
    }

    /**
     * Resend Verification Email.
     *
     * @param ResendVerificationTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerificationToken(ResendVerificationTokenRequest $request): \Illuminate\Http\JsonResponse
    {
        $admin = Admin::whereEmail($request->email)->first();

        abort_if($admin->email_verified_at, Response::HTTP_CONFLICT, 'This account had already been verified');

        $admin->sendEmailVerificationNotification();

        return $this->successResponse($admin, "Verification Token Resent to {$request->email}");
    }

    /**
     * Fetch My(Authenticated User) Profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchAuthenticatedadminProfile()
    {
        $admin = auth('admin')->user();

        return $this->successResponse(['admin' => $admin], 'admin Profile Returned Successfully');
    }

    /**
     * Send forgot password email.
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $admin = Admin::where("email", $request->email)->first();
        $otp = Otp::digits(5)->expiry(10)->generate($admin->email);
        $data = array("email" => $admin->email, "otp" => $otp, "first_name" => ucfirst($admin->first_name), "template" => "forgot-password", "subject" => "Forgot Password Link");
        //queue job
        SendEmail::dispatch($data)->delay(now()->addSeconds(5));

        return $this->successResponse($admin, 'Forgot password link sent successfully');
    }

    /**
     * Handle reset password.
     *
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $verify = Otp::digits(5)->expiry(10)->check($request->otp, $request->email);

        if (!$verify){
            return $this->failureResponse('invalid/expired OTP');
        }

        $user = Admin::query()->whereEmail($request->email)->first();

        $user->password = bcrypt($request->password);

        $user->save();

        return $this->successResponse($user, 'Password reset was successful');
    }

       /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['status' => 'success', 'message' => 'Admin successfully signed out'], 200);
    }

     /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ], 200);
    }

        /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }
    public function paymentSuccess(Request $request)
    {
        return view('payment');
    }
}
