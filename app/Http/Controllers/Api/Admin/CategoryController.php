<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\CategoryController\CreateRequest;
use App\Http\Requests\Api\Admin\CategoryController\UpdateRequest;
use App\Models\Category;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    use SendApiResponse;

    /**
     * Create Category.
     *
     * @param CreateRequest $request
     * @return void
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'description' => ['required', 'string']
        ]);
        if ($validator->fails()) {
            return  response()->json(
                [
                    'status' => 'failed',
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()
                ],
                422
            );
        }
        $category = Category::create([
            'title' => $request->name,
            'description' => $request->description

        ]);

        return $this->successResponse($category, 'Category Created Successfully');
    }

    /**
     * Update Category.
     *
     * @param UpdateRequest $request
     * @param Category $category
     * @return void
     */
    public function update(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'description' => ['required', 'string']
        ]);
        if ($validator->fails()) {
            return  response()->json(
                [
                    'status' => 'failed',
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()
                ],
                422
            );
        }
        $category = $category->update([
            'title' => $request->name,
            'description' => $request->description
        ]);

        return $this->successResponse($category, 'Category Updated Successfully');
    }

    /**
     * Show Category.
     *
     *
     * @param Category $category
     * @return void
     */
    public function show(Category $category)
    {
        return $this->successResponse($category);
    }

    /**
     * Delete Category.
     *
     * @param Category $category
     * @return void
     */
    public function delete(Category $category)
    {
        $category->delete();

        return $this->successResponse(null, null, Response::HTTP_NO_CONTENT);
    }
}
