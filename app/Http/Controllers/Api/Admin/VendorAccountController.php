<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\VendorAccountController\ActivateOrDeactivateRequest;
use Illuminate\Http\Request;
use App\Models\Vendor;
use App\Traits\SendApiResponse;

class VendorAccountController extends Controller
{
    use SendApiResponse;

    /**
     * Activate/Deactivate A Vendor Profile.
     *
     * @param ActivateOrDeactivateRequest $request
     * @return void
     */
    public function activateOrDeactivateAccount(ActivateOrDeactivateRequest $request)
    {
        $vendorAccount = Vendor::find($request->vendor_id);

        $vendorAccount->is_closed = !$vendorAccount->is_closed;

        $vendorAccount->save();

        $vendorAccount->refresh();

        $status = $vendorAccount->is_closed ? 'Activated' : 'De-Activated';

        return $this->successResponse($vendorAccount, "User Account ".  $status . " successfully");
    }
    public function fecthVendors(Request $request)
    {
        $vendors = Vendor::get();
        return $this->successResponse($vendors, 'Vendors Fecthed Successfully');
    }
}
