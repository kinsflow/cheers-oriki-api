<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\CelebrantAccountController\ActivateOrDeactivateRequest;
use App\Models\User;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;

class CelebrantAccountController extends Controller
{
    use SendApiResponse;

    /**
     * Activate/Deactivate A Celebrant Profile.
     *
     * @param ActivateOrDeactivateRequest $request
     * @return void
     */
    public function activateOrDeactivateAccount(ActivateOrDeactivateRequest $request)
    {
        $userAccount = User::find($request->user_id);

        $userAccount->is_closed = !$userAccount->is_closed;

        $userAccount->save();

        $userAccount->refresh();

        $status = $userAccount->is_closed ? 'Activated' : 'De-Activated';
        
        return $this->successResponse($userAccount, "User Account ".  $status . " successfully");
    }
}
