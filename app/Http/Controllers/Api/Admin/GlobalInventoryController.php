<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\GlobalInventoryController\CreateRequest;
use App\Http\Requests\Api\Admin\GlobalInventoryController\DeleteRequest;
use App\Models\GlobalVendor;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GlobalInventoryController extends Controller
{
    use SendApiResponse;

    /**
     * Return all vendor
     *
     * @return void
     */
    public function index()
    {
        $globalVendor = GlobalVendor::paginate();

        return $this->successResponse($globalVendor);
    }

    /**
     * Create Global Vendor
     *
     * @param CreateRequest $request
     * @return void
     */
    public function create(CreateRequest $request)
    {
        $globalVendor = GlobalVendor::create([
            'name' => $request->name,
            'url' => $request->url
        ]);

        return $this->successResponse($globalVendor, 'Global Vendor Created Successfully');
    }

    /**
     * Delete global Vendor.
     *
     * @param DeleteRequest $request
     * @return void
     */
    public function delete(GlobalVendor $globalVendor)
    {
        $globalVendor->delete();

        return $this->successResponse(null, null, Response::HTTP_NO_CONTENT);
    }
}
