<?php

namespace App\Http\Controllers\Api\Friends;

use App\Enums\FriendStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Friends\FriendController\RequestActionRequest;
use App\Http\Requests\Api\Friends\FriendController\SendFriendRequest;
use App\Models\Contact;
use App\Models\Friend;
use App\Models\User;
use App\Notifications\Friends;
use App\Traits\SendApiResponse;
use App\Traits\SendFCMNotification;
use hanneskod\classtools\Iterator\Filter\WhereFilter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;


class FriendController extends Controller
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * The Ability of a user sending friend request
     * to another user.
     *
     * @param SendFriendRequest $request
     */
    public function sendFriendRequest(SendFriendRequest $request)
    {
        if ($request->friend_id == Auth::id()) {
            return $this->failureResponse("You cannot send a request to yourself", Response::HTTP_FORBIDDEN);
        }

        // did I(logged-in user) ever send this friend_id a request
        $haveISentARequest = Friend::where('first_user', Auth::id())->where('second_user', $request->friend_id);

        // did I(logged-in user) ever got a request from this friend_id
        $haveIReceivedARequest = Friend::where('first_user', $request->friend_id)->where('second_user', Auth::id());

        if ($haveISentARequest->where('acted_user', Auth()->id())->first()) {
            return $this->failureResponse("A connection exists already", Response::HTTP_CONFLICT);
        }
        // did this friend_id once send me a request that i am yet to attend to
        if ($haveIReceivedARequest->where('status', FriendStatus::PENDING)->first()) {
            return $this->failureResponse("Hey!! you have a pending friend request from this user, kindly accept or decline on your list of pending friend request", Response::HTTP_CONFLICT);
        }

        // I once sent this friend_id a friend request but they are yet to attend to it
        if ($haveISentARequest->where('status', FriendStatus::PENDING)->first()) {
            return $this->failureResponse("Hey!! you have once sent a friend request to this user, wait while they attend to it", Response::HTTP_CONFLICT);
        }


        // Is this friend_id a blocked connection
        if (
            $haveIReceivedARequest->where('status', FriendStatus::BLOCKED)->first() ||
            $haveISentARequest->where('status', FriendStatus::BLOCKED)->first()
        ) {
            return $this->failureResponse("Hey!! you have a blocked connection with this user");
        }

        // Is this friend_id a friend already
        if (
            $haveIReceivedARequest->where('status', FriendStatus::CONFIRMED)->first() ||
            $haveISentARequest->where('status', FriendStatus::CONFIRMED)->first()
        ) {
            return $this->failureResponse("Hey!! this user is a friend already");
        }

        Friend::create([
            'first_user' => Auth::id(),
            'second_user' => $request->friend_id,
            'acted_user' => Auth::id(),
            'status' => FriendStatus::PENDING
        ]);
        // $msg = $RequestSender-

        $RequestRecipient = User::find($request->friend_id);
        $RequestSender = Auth::user();

        $message = $RequestSender->first_name.''.$RequestSender->lastname.' sent you a friend request';
        $title = 'Friend Request';

        $RequestRecipient->notify(new Friends($message, $title));
        $this->sendSingleDevice('Friend Request', $message, 'FRIEND_CONNECTION', $request->friend_id);

        return $this->successResponse(null, "Friend Request sent successfully");
    }

    /**
     * Get a list of a user friend connections.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function myFriends(Request $request)
    {
        if($request->filter['status'] == 'pending')
        {
            $friends = QueryBuilder::for(Friend::class)
            ->allowedFilters([
                'status'
            ])
            ->with(['firstUser', 'secondUser'])
            ->where( function ($query){
                $query->where('second_user', Auth::id());
            })
            ->paginate($request->per_page);

        return $this->successResponse($friends);
        }
        $friends = QueryBuilder::for(Friend::class)
            ->allowedFilters([
                'status'
            ])
            ->with(['firstUser', 'secondUser'])
            ->where( function ($query){
                $query->where('first_user', Auth::id())
                ->orWhere('second_user', Auth::id());
            })
            // ->where('first_user', Auth::id())
            // ->orWhere('second_user', Auth::id())
            ->paginate($request->per_page);

        return $this->successResponse($friends);
    }

    /**
     * This endpoint gives the option of
     * accepting/declining/blocking/unblocking
     * a friend request.
     *
     * @param RequestActionRequest $request
     */
    public function requestAction(RequestActionRequest $request)
    {
        $friend = Friend::find($request->friend_list_id);
        $user = Auth::user();


        $notificationReceiverId = $friend->first_user == auth()->id() ? $friend->second_user : $friend->first_user;

        if (!($friend->first_user == auth()->id()) && !($friend->second_user == auth()->id())) {
            return $this->failureResponse("The Connection ID is not related to you", Response::HTTP_FORBIDDEN);
        }


        // decline a friend request
        if ($request->action === FriendStatus::DECLINED) {
            if ($friend->status !== FriendStatus::PENDING) {
                return $this->failureResponse("Hey!! you cannot decline a request like this as it is not a pending request");
            }

            $friend->delete();

            $this->sendSingleDevice('connection action', "your request has been {$request->action}", 'CONNECTION_REQUEST', $notificationReceiverId);

                $Recipient = User::find($friend->first_user);
                $message = 'Your friend request to '.$user->username. ' has been '.$request->action.'';

                $Recipient->notify(new Friends($message));

                return $this->successResponse(null, "Friend Request Declined Successfully");
        }

        // confirm a request
        if ($request->action === FriendStatus::CONFIRMED) {
            if ($friend->status !== FriendStatus::PENDING) {
                return $this->failureResponse("Hey!! you had no pending friend connection with this user");
            }
            if($friend->first_user == auth()->id())
            {
                return $this->failureResponse("You cannot accept this request, you are the sender");
            }

            $friend->status = FriendStatus::CONFIRMED;

            $friend->save();

            // Delete my record from the acted_user's contact list since i am now a friend.
            Contact::query()
                ->where(['email' => auth()->user()->email, 'user_id' => $friend->acted_user])
                ->orWhere(['phone' => auth()->user()->phone, 'user_id' => $friend->acted_user])
                ->delete();

            $this->sendSingleDevice('connection action', "your request has been {$request->action}", 'CONNECTION_REQUEST', $notificationReceiverId);

                $Recipient = User::find($friend->first_user);
                $message = 'Your friend request to '.$user->username. ' has been '.$request->action.'';

                $Recipient->notify(new Friends($message));

                return $this->successResponse(null, "Friend Request Accepted Successfully");
        }

        // block a user
        if ($request->action === FriendStatus::BLOCKED) {
            if ($friend->status !== FriendStatus::CONFIRMED) {
                return $this->failureResponse("Hey!! you cannot block a user you had no connection with before");
            }

            $friend->status = FriendStatus::BLOCKED;
            $friend->acted_user = Auth::id();

            $friend->save();

            // $this->sendSingleDevice('connection action', "your request has been {$request->action}", 'CONNECTION_REQUEST', $notificationReceiverId);
            return $this->successResponse(null, "Friend Blocked Successfully");
        }

        // unblock a user
        if ($request->action === FriendStatus::UNBLOCKED) {
            if ($friend->status !== FriendStatus::BLOCKED) {
                return $this->failureResponse("Hey!! you cannot unblock a user you had no blocked connection with before");
            }

            if ($friend->acted_user !== Auth::id()) {
                return $this->failureResponse("Hey!! you did not block this user, hence, you cannot unblock this user");
            }

            $friend->status = FriendStatus::CONFIRMED;
            $friend->acted_user = Auth::id();

            $friend->save();

            $this->sendSingleDevice('connection action', "you request has been {$request->action}", 'CONNECTION_REQUEST', $notificationReceiverId);
            return $this->successResponse(null, "Friend Unblocked Successfully");
        }
    }

    public function searchUsers(Request $request)
    {
       $user_id = auth()->id();
       $users = User::where('last_name', 'like', "%$request->search%")
            ->orWhere('first_name', 'like', "%$request->search%")
            ->orWhere('username', 'like', "%$request->search%")->select('id', 'first_name', 'last_name', 'username')
            ->paginate();
        foreach($users as $user)
        {

            if(!Friend::where('first_user', $user_id)->where('second_user', $user->id)->orWhere('first_user', $user->id)->where('second_user', $user_id)->exists())
            {
             $user['connected_status'] = 'unconnected';
            }else{
            $connect = Friend::where('first_user', $user_id)->where('second_user', $user->id)->orWhere('first_user', $user->id)->where('second_user', $user_id)->first();
            if($connect->status == 'confirmed')
            {
            $user['connected_status'] = 'connected';
            }
            elseif($connect->status == 'pending')
            {
            $user['connected_status'] = 'requested';
            }
            elseif($connect->status == 'blocked')
            {
            $user['connected_status'] = 'blocked';
            }
            }

        }
        if(sizeof($users) == 0 )
        {
        return $this->successResponse('No User found');
        }
        return $this->successResponse($users, 'Search Result fetched successful');
    }


}
