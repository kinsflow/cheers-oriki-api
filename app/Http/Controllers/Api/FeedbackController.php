<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\FeedbackController\createFeedbackRequest;
use App\Models\Feedback;
use App\Traits\SendApiResponse;

class FeedbackController extends Controller
{
    use SendApiResponse;

    /**
     * User create report.
     *
     * @param createFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFeedbacks(createFeedbackRequest $request)
    {
        $feedback = Feedback::create([
            'title' => $request->title,
            'user_id' => auth()->id(),
            'body' => $request->body
        ]);

        return $this->successResponse($feedback, "Feedback made successfully");
    }

    /**
     * Get all reports made by a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserFeedbacks()
    {
        return $this->successResponse(auth()->user()->feedbacks);
    }
}
