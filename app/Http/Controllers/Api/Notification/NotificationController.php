<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\SendApiResponse;
use Illuminate\Http\Response;

class NotificationController extends Controller
{
    use SendApiResponse;
    //
    public function index(){
        $user = Auth()->user();
        $userNotification = $user->notifications;
        $unreadNotificationsCount = $user->unreadNotifications->count();

        return $this->successResponse(['data' => $userNotification, 'unreadNotificationCount' => $unreadNotificationsCount], 'Notifications Returned Successfully');
    }

    public function markNotifcationAsRead($id){
        $user = Auth()->user();
        $unReadNotification = $user->unreadNotifications->where('id', $id)->first();
        if(!$unReadNotification){
        return $this->failureResponse("Notification Not Found", Response::HTTP_NOT_FOUND);
        }

        $unReadNotification->markAsRead();
        return $this->successResponse($unReadNotification, 'Notification Returned Successfully');
    }

    public function readAllNotification(Request $request)
    {
        $user = Auth()->user();
        $user->unreadNotifications->markAsRead();
        return $this->successResponse( 'All notification Read Successfully');
    }
}
