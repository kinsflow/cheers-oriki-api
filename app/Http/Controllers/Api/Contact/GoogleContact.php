<?php

namespace App\Http\Controllers\Api\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Contact\GoogleContact\FetchContactRequest;
use App\Models\Contact;
use App\Traits\SendApiResponse;
use Google_Client;
use Google_Service_PeopleService;

class GoogleContact extends Controller
{
    private Contact $contact;
    use SendApiResponse;

    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * This method returns all google contact of a user
     * and it filters contacts with emails alone.
     *
     * @param FetchContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchContact(FetchContactRequest $request)
    {
        try {
            $optParams = [
                'personFields' => 'names,emailAddresses,phoneNumbers,birthdays,photos',
                'pageSize' => 2000,
            ];

            $client = new Google_Client();
            $client->setAccessToken($request->token);

            $service = new Google_Service_PeopleService($client);

            if ($request->has('next_page_token')) {
                $optParams['pageToken'] = $request->get('next_page_token');
                $results = $service->people_connections->listPeopleConnections('people/me', $optParams);
            } else {
                $results = $service->people_connections->listPeopleConnections('people/me', $optParams);
            }

            $filtered_results = array_filter(collect($results)->toArray(), function ($result) {
                return $result['emailAddresses'];
            });

            $data = [
                'next_page_token' => $results->getNextPageToken(),
                'has_next_page' => (bool)$results->getNextPageToken(),
                'contacts' => $filtered_results,
            ];
            return $this->successResponse($data);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage());
        }
    }
}
