<?php

namespace App\Http\Controllers\Api\Contact;

use App\Enums\FriendStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Contact\ContactController\FetchARegisteredContactInformationRequest;
use App\Http\Requests\Api\Contact\ContactController\ImportContactRequest;
use App\Jobs\SendImportedContactInvite;
use App\Models\Contact;
use App\Models\Friend;
use App\Models\User;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    use SendApiResponse;

    public function importContact(ImportContactRequest $request)
    {
        // get list of all phone numbers in the user model
        $userPhones = (User::select('phone')->get())->toArray();

        $userPhonesWithoutArrayKeys = array_map(function ($phones) {
            return $phones['phone'];
        }, $userPhones);

        // prepare user import data for the database
        $contactsToImport = collect($request->validated())->map(function ($contact) use ($userPhonesWithoutArrayKeys) {
            return [
                "last_name" => explode(" ", $contact["name"])[0],
                "first_name" => explode(" ", $contact["name"])[1] ?? null,
                "user_id" => Auth::id(),
                "email" => $contact["email"] ?? null,
                "phone" => $contact["phone"],
                "birthday" => $contact["birthday"] ?? null,
                "is_registered" => in_array($contact["phone"], $userPhonesWithoutArrayKeys),
                "created_at" => now(),
                "updated_at" => now()
            ];
        });

        // create or update the record based on user_id and email
        DB::transaction(function () use ($contactsToImport, $request) {
            Contact::upsert($contactsToImport->toArray(), ['user_id', 'phone']);

            $importedContactEmail = collect($request->validated())->pluck('email');

            // Contact Invite Notification Jobs
            //            SendImportedContactInvite::dispatch($importedContactEmail->toArray()); @todo to be reviewed by the stake holders since there is no email for all client
        });

        return $this->successResponse($contactsToImport);
    }

    /**
     * This method is intended to delete an invited
     * contact from a user(inviter) contact list,
     * and make such contact a friend of the user(inviter).
     *
     * @param Request $request
     */
    public function manageRegisteringContacts(Request $request, User $user)
    {
        $inviterId = decrypt($request->invite_token);

        DB::transaction(function () use ($request, $inviterId, $user) {
            // create friends record
            Friend::create([
                'first_user' => $inviterId,
                'second_user' => $user->id,
                'acted_user' => $inviterId,
                'status' => FriendStatus::CONFIRMED
            ]);

            $invitedContactInstance = Contact::whereEmail($request->email)
                ->whereUserId($inviterId)->first();

            // delete user from contact list
            $invitedContactInstance->delete();
        });
    }

    /**
     * Fetch Contact of current user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchAUserContact(Request $request)
    {
        $myContacts = Contact::whereUserId(Auth::id())->paginate($request->per_page);

        return $this->successResponse($myContacts);
    }

    /**
     * Return the details of a registered contact from
     * the user model.
     *
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchARegisteredContactInformation(Contact $contact)
    {
        if (!$contact->is_registered) {
            return $this->failureResponse('This Contact is not a registered user');
        }

        $userInformation = User::wherePhone($contact->phone)->first();

        if (!$userInformation) {
            return $this->failureResponse('Contact Info cannot be retrieved because the phone number seems not to align with any of the registered record');
        }

        return $this->successResponse($userInformation, 'Registered contact information returned successfully');
    }

    /**
     * Handle Search With Contact That Belongs To A User.
     *
     * @param Request $request
     * @return void
     */
    public function searchContact(Request $request)
    {
        $authId = auth()->id();

        $contacts = Contact::whereUserId($authId)
            ->where('last_name', 'like', "%$request->search%")
            ->orWhere('first_name', 'like', "%$request->search%")
            ->orWhere('email', 'like', "%$request->search%")
            ->paginate();

        return $this->successResponse($contacts, 'Search Result fetched successful');
    }
}
