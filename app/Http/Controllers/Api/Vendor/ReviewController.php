<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Models\Review;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    use SendApiResponse;

    /**
     * The review that belong to an inventory.
     *
     * @param Request $request
     * @param Inventory $inventory
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnInventoryReview(Request $request, Inventory $inventory)
    {
        $reviews = Review::query()
            ->whereModel('inventory')
            ->whereModelId($inventory->id)
            ->paginate($request->per_page);

        return $this->successResponse($reviews, 'Reviews returned successfully');
    }
}
