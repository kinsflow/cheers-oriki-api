<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Vendor\InventoryController\IndexRequest;
use App\Http\Requests\Api\Vendor\InventoryController\StoreRequest;
use App\Http\Requests\Api\Vendor\InventoryController\UpdateRequest;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\Wishlist;
use App\Traits\SendApiResponse;
// use AWS\CRT\HTTP\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;

use function PHPUnit\Framework\isEmpty;

class InventoryController extends Controller
{
    use SendApiResponse;

    /**
     * Return Inventories that belongs to a signed
     * in vendor and filter by category_id.
     *
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request): JsonResponse
    {
        $inventories = QueryBuilder::for(Inventory::query())
            ->allowedFilters([
                'category_id',
            ])
            ->whereVendorId(auth('vendor')->id())
            ->orderBy('id', 'desc')
            ->paginate($request->per_page);

        return $this->successResponse($inventories, 'Inventories Returned Successfully');
    }

    /**
     * Create Inventory By A Vendor.
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $validator = Validator::make($request->all(), [

                'title' => ['required', 'string'],
                'description' => ['nullable', 'string'],
                'category_id' => ['required', 'exists:categories,id'],
                'gallery' => ['nullable', 'array'],
                'gallery.*' => ['mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime,image/png,image/jpeg'] ,
                'price' => ['required','between:0,9999999.99'],
                'active' => ['required','boolean'],
                'stock' => ['required', 'integer']
        ]);
        if($validator->fails())
        {
            $errors = $validator->errors();
            foreach ($errors->all() as $message)
            {
                return response()->json([
                   'status' => 'Validation Errors',
                   'message' => $message
                ], 422);
            }
        }
        $sku = auth('vendor')->user()->vendor_name .rand(10000, 90000);;
        $createInventory = Inventory::create([
            'title' => $request->title,
            'description' => $request->description,
            'vendor_id' => auth('vendor')->id(),
            'category_id' => $request->category_id,
            'price' => $request->price,
            'active' => $request->active,
            'stock' => $request->stock,
            'sku' => $sku
        ]);

        if ($request->gallery) {
            $createInventory->addMultipleMediaFromRequest(['gallery'])
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollections::INVENTORYGALLERY);
                });
        }

        return $this->successResponse($createInventory, 'Inventory Created Successfully');
    }

    /**
     *
     * return a single inventory resource.
     *
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function show(Inventory $inventory)
    {
        return $this->successResponse($inventory->load('reviews.user'), 'Inventory Returned Successfully');
    }

    /**
     * Update Inventory Resource.
     *
     * @param Inventory $inventory
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(Inventory $inventory, UpdateRequest $request)
    {
        $inventory->title = $request->title;
        $inventory->description = $request->description;
        $inventory->category_id = $request->category_id;
        $inventory->price = $request->price;

        if ($request->gallery) {
            $inventory->media()->delete();

            $inventory->refresh();

            $inventory->addMultipleMediaFromRequest(['gallery'])
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollections::INVENTORYGALLERY);
                });
        }

        $inventory->save();

        return $this->successResponse($inventory, 'Inventory Updated Successfully');
    }

    /**
     * Delete a specific inventory resource from storage.
     *
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function delete(Inventory $inventory)
    {
        $inventory->delete();
        return $this->successResponse($inventory, '', Response::HTTP_NO_CONTENT);
    }

    /**
     * Get all categories.
     *
     * @return void
     */
    public function fetchCategories()
    {
        $categories = Category::all();

        return $this->successResponse($categories, '', Response::HTTP_OK);
    }

    public function allInventories(IndexRequest $request): JsonResponse
    {
        $inventories = QueryBuilder::for(Inventory::query())
            ->allowedFilters([
                'category_id',
            ])
            ->orderBy('id', 'desc')
            ->paginate($request->per_page);

        return $this->successResponse($inventories, 'All Inventories Returned Successfully');
    }
    public function getCategories()
    {
        $vendorId = auth('vendor')->id();
       $inventories = Inventory::where('vendor_id', $vendorId)->get();
        $data = [];
        $categories = [];
        foreach ($inventories as $inventory)
        {
            if(!in_array(intval($inventory->category->id), $data))
            {
            $data[] = $inventory->category->id;
            }
        }
        foreach($data as $category)
        {
          $utilized_categories =  Category::where('id', $category)->first();
          $categories[] = $utilized_categories;
        }
        $no_of_utilized_categories = sizeof($data);
        return $this->successResponse([$categories, 'no_of_utilized_categories' => $no_of_utilized_categories], 'All Inventories Returned Successfully');

    }

    public function getVendorAnalysis()
    {
        $vendorId = auth('vendor')->id();
        $inventories =  Inventory::where('vendor_id', $vendorId)->get();
        $wishlistInventories = [];
        foreach ($inventories as $inventory)
        {
            $items = Wishlist::where('model_id', $inventory->id)->with(['product', 'user'])->get();
            // $wishlistInventories[] = $items;
            array_push($wishlistInventories, $items);
        }
        return $this->successResponse($wishlistInventories, 'All Inventories Returned Successfully');

    }

    public function inventoryMultipleFunction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'action' => ['required'],
            'ids' => ['required'],
        ]);
        if($validator->fails())
        {
            $errors = $validator->errors();
            foreach ($errors->all() as $message)
            {
                return response()->json([
                   'status' => 'Validation Errors',
                   'message' => $message
                ], 422);
            }
        }
        $vendorId = auth('vendor')->id();
            if($request->action == 'DELETE_INVENTORY')
            {
                foreach ($request->ids as $id)
                {
                Inventory::where('id', $id)->where('vendor_id', $vendorId)->delete();
                }
                return $this->successResponse('Inventories Deleted Successfully');
            }elseif($request->action == 'ACTIVATE_INVENTORY')
            {
                foreach ($request->ids as $id)
                {
                Inventory::where('id', $id)->where('vendor_id', $vendorId)->update([
                    'active' => true
                ]);
            }
                return $this->successResponse('Inventories Activated Successfully');

            }elseif($request->action == 'DEACTIVATE_INVENTORY')
            {
                foreach ($request->ids as $id)
                {
                Inventory::where('id', $id)->where('vendor_id', $vendorId)->update([
                    'active' => false
                ]);
            }
            return $this->successResponse('Inventories Deactivated Successfully');

            }
            elseif($request->action == 'ZERO_STOCK')
            {
                foreach ($request->ids as $id)
                {
                Inventory::where('id', $id)->where('vendor_id', $vendorId)->update([
                    'stock' => 0
                ]);
                }
                return $this->successResponse('Inventories has been zeroed');

            }
            else{
                return $this->failureResponse('Invalid action code');
            }
        }

        public function searchInventory(Request $request)
        {
          $vendorId = auth('vendor')->id();
          $search_result = Inventory::where('vendor_id', $vendorId)->get()
          ->andwhere('name', 'like', "%$request->search%")
          ->orwhere('sku', 'like', "%$request->search%");
          return $this->successResponse($search_result, 'Search results returned successfully');

        }
    }

