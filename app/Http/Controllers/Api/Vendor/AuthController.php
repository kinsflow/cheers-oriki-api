<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Vendor\AuthController\ForgotPasswordRequest;
use App\Http\Requests\Api\Vendor\AuthController\ResendVerificationTokenRequest;
use App\Http\Requests\Api\Vendor\AuthController\ResetPasswordRequest;
use App\Http\Requests\Api\Vendor\AuthController\SignInRequest;
use App\Http\Requests\Api\Vendor\AuthController\SignUpRequest;
use App\Http\Requests\Api\Vendor\AuthController\updateVendorProfileRequest;
use App\Http\Requests\Api\Vendor\AuthController\VerifyRequest;
use App\Jobs\SendEmail;
use App\Models\Vendor;
use App\Traits\SendApiResponse;
use Aws\Api\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Tzsk\Otp\Facades\Otp;

class AuthController extends Controller
{
    use SendApiResponse;

    /**
     * Sign Up / Register as a Vendor.
     *
     * @param SignUpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(SignUpRequest $request)
    {
    $validator = FacadesValidator::make($request->all(), [
        'email' => 'required|email|unique:vendors,email',
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'vendor_name' => 'required|string',
        'phone' => 'required|string|unique:vendors,phone',
        'password' => 'required|string|confirmed|min:8',
        'profile_picture' => ['image', 'mimetypes:image/png,image/jpeg,image/jpg']
       ]);
       if ($validator->fails()) {
           return response()->json(
               [
                   'status' => 'failed',
                   'message' => 'The given data was invalid.',
                   'errors' => $validator->errors()
               ],
               422
           );
       }
       $vendor = Vendor::create([
           'first_name' => $request->first_name,
           'last_name' => $request->last_name,
        'vendor_name' => $request->vendor_name,
           'phone' => $request->phone,
           'email' => $request->email,
           'username' => 'Cheers Vendor',
           'password' => bcrypt($request->password)
        ]);

        if ($request->profile_picture) {
            $vendor->addMediaFromRequest('profile_picture')->toMediaCollection(MediaCollections::VENDORPROFILEPICTURE);
        }

        $vendor->sendEmailVerificationNotification();

        return $this->successResponse($vendor, 'Sign up successful. A verification code has being sent to your email, kindly use the code to verify your email');
    }

    /**
     * Sign in as a vendor.
     *
     * @param SignInRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(SignInRequest $request)
    {
        $vendor = Vendor::whereEmail($request->email)->first();

        if(!$vendor)
        {
                return response()->json([
                    'message' => 'Email does not exists in our record',
                    'error' => true
                ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $isEmailVerified = $vendor->email_verified_at;

        !($isEmailVerified) ? $vendor->sendEmailVerificationNotification(): null;

        if(!($isEmailVerified))
        {
            return response()->json([
                'message' => 'Account has not being verified check your email for verification code',
                 'error' => true
             ], Response::HTTP_FORBIDDEN);
        }

        if (!$vendor->is_closed){
            return $this->failureResponse("This account has been closed, reach out to admin to get your account activated", 403);
        }

        auth()->shouldUse('vendor');

        $token = auth()->attempt([
           'email' => $request->email,
           'password' => $request->password
        ]);

        if (!$token){
            return $this->failureResponse('Incorrect Email Or Password', Response::HTTP_UNAUTHORIZED);
        }

        return $this->successResponse(['token' => $token], 'Signed In Successfully');
    }

    /**
     * Verify Vendor's Email Address;
     *
     * @param VerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyEmail(VerifyRequest $request)
    {
        $verify = Otp::digits(6)->expiry(10)->check($request->token, $request->email);

        if (!$verify){
            return $this->failureResponse('invalid/expired access_token', Response::HTTP_FORBIDDEN);
        }

        $vendor = Vendor::whereEmail($request->email)->first();
        $vendor->markEmailAsVerified();

        return $this->successResponse(['vendor' => $vendor], 'Email Verified Successfully');
    }

    /**
     * Resend verification token to email maybe in case
     * of token expiry or non-delivery of first token
     * sent for obvious reason.
     *
     * @param ResendVerificationTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerificationToken(ResendVerificationTokenRequest $request): \Illuminate\Http\JsonResponse
    {
        $vendor = Vendor::whereEmail($request->email)->first();

        abort_if($vendor->email_verified_at, Response::HTTP_CONFLICT, 'This account had already been verified');

        $vendor->sendEmailVerificationNotification();

        return $this->successResponse($vendor, "Verification Token Resent to {$request->email}");
    }

    /**
     * Fetch My(Authenticated User) Profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchAuthenticatedVendorProfile()
    {
        $vendor = auth('vendor')->user();

        return $this->successResponse(['vendor' => $vendor], 'Vendor Profile Returned Successfully');
    }

    /**
     * Send forgot password email.
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $vendor = Vendor::where("email", $request->email)->first();
        $otp = Otp::digits(5)->expiry(10)->generate($vendor->email);
        $data = array("email" => $vendor->email, "otp" => $otp, "first_name" => ucfirst($vendor->first_name), "template" => "forgot-password", "subject" => "Forgot Password Link");
        //queue job
        SendEmail::dispatch($data)->delay(now()->addSeconds(5));

        return $this->successResponse($vendor, 'Forgot password link sent successfully');
    }

    /**
     * Handle reset password.
     *
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $verify = Otp::digits(5)->expiry(10)->check($request->otp, $request->email);

        if (!$verify){
            return $this->failureResponse('invalid/expired OTP');
        }

        $user = Vendor::query()->whereEmail($request->email)->first();

        $user->password = bcrypt($request->password);

        $user->save();

        return $this->successResponse($user, 'Password reset was successful');
    }

        /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['status' => 'success', 'message' => 'Vendor successfully signed out'], 200);
    }
         /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ], 200);
    }

        /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }
    public function updateVendorProfile(Request $request)
    {
        $validator = FacadesValidator::make($request->all(), [
             'email' => 'required|email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'vendor_name' => 'required|string',
            'phone' => 'required|string',
            'profile_picture' => 'image', 'mimetypes:image/png,image/jpeg,image/jpg'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()
                ],
                422
            );
        }
        $vendor_id = auth('vendor')->user()->id;

        $vendor = Vendor::find($vendor_id);

        $vendor->update($request->all());

        if ($request->has('profile_picture')) {
            $vendor->addMediaFromRequest('profile_picture')->toMediaCollection(MediaCollections::PROFILEPICTURE);
        }
        return $this->successResponse($vendor, "Vendor profile updated successfully");
    }

}
