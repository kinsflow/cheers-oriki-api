<?php

namespace App\Http\Controllers\Api\Profile;

use App\Enums\PaymentActionType;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\Flutterwave;
use App\Http\Requests\Api\Profile\WalletController\FundMyWalletRequest;
use App\Http\Requests\Api\Profile\WalletController\WithdrawFromMyWalletRequest;
use App\Models\Wallet;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Traits\SendFCMNotification;
use Illuminate\Support\Facades\Hash;

class WalletController extends Controller
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * @var Flutterwave
     */
    private $flutterwave;

    public function __construct(Flutterwave $flutterwave)
    {
        $this->flutterwave = $flutterwave;
    }

    /**
     * Fund wallet by paying through flutterwave
     */
    public function fundMyWallet(FundMyWalletRequest $request)
    {
        $wallet = Wallet::whereUserId(Auth::id())->first();

        if (!$wallet) {
            $wallet = Wallet::create([
                'user_id' => Auth::id()
            ]);
        }

        $paymentData = $this->flutterwave->createPayment($wallet, $request);

        $sendDataToFlutterWave = json_decode($this->flutterwave->sendPaymentToFlutterWave($paymentData)->getContents())->data;

        return $this->successResponse($sendDataToFlutterWave, 'Payment initialized successfully');
    }

    /**
     * Withdraw from wallet balance
     * @param WithdrawFromMyWalletRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function withdrawFromMyWallet(WithdrawFromMyWalletRequest $request)
    {
        $wallet = Wallet::whereUserId(Auth::id())->first();

        if (!$wallet) {
            return $this->failureResponse("You dont have a wallet record on this app, fund your wallet to have a wallet record");
        }

        if (is_null($wallet->balance) || ($wallet->balance < $request->amount)) {
            return $this->failureResponse("Insufficient wallet balance");
        }
        $user_password = auth()->user()->password;
        if(!Hash::check($request->password, $user_password))
        {
            return $this->failureResponse("You have entered an incorrect password");
        }

        try {
            $transfer = json_decode($this->flutterwave->receivePaymentFromFlutterwave($request)->getContents())->data;

            // create withdrawal history
            $paymentData = $this->flutterwave->createWithrawalRecord($wallet, $request, $transfer);

            // Update wallet balance upon successful transaction
            $wallet->last_withdrawn = Carbon::parse($transfer->created_at);
            $wallet->balance -= $request->amount;
            $wallet->save();
            $this->sendSingleDevice('Wallet Action', "Your wallet has been debited with ₦{$request->amount}", 'WALLET_ACTION', Auth()->id());


            return $this->successResponse($paymentData, 'Withdrawal Request is being processed already');
        } catch (\Exception $exception) {
            return $this->failureResponse(
                $exception->getMessage(),
                gettype($exception->getCode()) == "integer" ?
                    $exception->getCode() :
                    Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get Current Wallet Detail Of A User.
     *
     * @return mixed
     */
    public function walletBalance()
    {
        if (!Auth::user()->wallet){
            return $this->failureResponse("Hey!! You've got no wallet record, fund your wallet to have one");
        }
        return $this->successResponse(Auth::user()->wallet, "Wallet Detail Returned successfully");
    }
}
