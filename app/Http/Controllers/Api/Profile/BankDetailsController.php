<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Profile\BankDetailsController\ComputeBankDetailsRequest;
use App\Models\BankDetail;
use App\Traits\SendApiResponse;
use Illuminate\Support\Facades\Hash;

class BankDetailsController extends Controller
{
    use SendApiResponse;

    /**
     * Update or create user bank details.
     *
     * @param ComputeBankDetailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function computeBankDetails(ComputeBankDetailsRequest $request)
    {

        $user_password = auth()->user()->password;
        if(!Hash::check($request->password, $user_password))
        {
            return $this->failureResponse("You have entered an incorrect password");
        }
        $updateOrCreateUserBankDetail = BankDetail::query()
            ->updateOrCreate(['user_id' => auth()->id()],
            [
                'account_bank' => $request->account_bank_code,
                'account_number' => $request->account_number
            ]);

        return $this->successResponse($updateOrCreateUserBankDetail, 'Bank details updated successfully');
    }

    /**
     * Return user bank details.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchBankDetail()
    {
        $bankDetail = auth()->user()->bankDetails;

        return $this->successResponse($bankDetail, 'Bank details returned successfully');
    }
}
