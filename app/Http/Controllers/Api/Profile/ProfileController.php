<?php

namespace App\Http\Controllers\Api\Profile;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Profile\ProfileController\UpdateUserAddressRequest;
use App\Http\Requests\Api\Profile\ProfileController\UpdateUserProfileRequest;
use App\Models\Address;
use App\Models\Friend;
use App\Models\User;
use App\Traits\SendApiResponse;
use App\Traits\SendFCMNotification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;

class ProfileController extends Controller
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * Return Authenticated User Detail.
     * @return JsonResponse
     */
    public function getCurrentUserProfile()
    {
        $user = Auth::user();

        return $this->successResponse($user);
    }

    /**
     * Get Other User Profile.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function getOtherUserProfile(User $user)
    {
        return $this->successResponse($user);
    }

    /**
     * Update Your Profile as a user.
     *
     * @param UpdateUserProfileRequest $request
     * @return JsonResponse
     */
    public function updateUserProfile(UpdateUserProfileRequest $request): JsonResponse
    {
        $user_id = Auth::id();

        $user = User::find($user_id);

        $user->update($request->validated());

        if ($request->has('avatar')) {
            $user->addMediaFromRequest('avatar')->toMediaCollection(MediaCollections::PROFILEPICTURE);
        }

        // $this->sendMultipleDevice("Profile Update", "{$user->last_name}  {$user->first_name} Just updated their profile", "PROFILE_UPDATE");

        return $this->successResponse($user, "User profile updated successfully");
    }

    /**
     * Updated the address of a user.
     *
     * @param UpdateUserAddressRequest $request
     * @return JsonResponse
     */
    public function updateUserAddress(UpdateUserAddressRequest $request): JsonResponse
    {
        $user_id = Auth::id();

        $address = Address::whereUserId($user_id)->first() ?? new Address();

        $address->forceFill([
            'street' => $request->street,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'postal_code' => $request->postal_code,
            'user_id' => $user_id
        ])->save();

        return $this->successResponse($address, "Address updated successfully");
    }

    /**
     * User close self account.
     *
     * @return JsonResponse
     */
    public function closeAccount()
    {
        $authUser = Auth::user();

        $authUser->closeAccount();

        return $this->successResponse($authUser, "Account closed successfully");
    }

    public function updateUser(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $user->date_of_birth = $request->date_of_birth;
        $user->save();
        return response()->json([
            'message' => 'Updated Successfully'
        ]);
    }
    public function updateUser2(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $user->email = $request->nemail;
        $user->phone = $request->phone;
        $user->save();
        return response()->json([
            'message' => 'All user details Updated Successfully'
        ]);
    }

    public function birthd(Request $request)
    {


        $bu = User::whereDay('date_of_birth', Carbon::now())->whereMonth('date_of_birth', Carbon::now())->get();

        foreach ($bu as $b)
        {
            $msg = 'Today is ' .$b->username .' birthday, Pleae wish him well';
            // $this->sendMultipleDevice('Birthday Reminder', $msg, 'BIRTHDAY');
        }

        // return $msg;


    }
    // public function morningBirthda



//     $bu = User::whereDay('date_of_birth', Carbon::now())->whereMonth('date_of_birth', Carbon::now())->get();
//     $user_id = [];
//     $friendConnect = [];
//     foreach($bu as $b)
//     {
//         $user_id[]= $b->id;
//         $friendConnections = Friend::query()
//         ->where('first_user', $b->id)
//         ->orWhere('second_user', $b->id)
//         ->get(['first_user', 'second_user']);
//         $friendConnections['username'] = $b->username;
//         $friendConnections['id']= $b->id;

//         $friendConnect[] = $friendConnections;
//         // array_merge($friendConnect, array('username' => $b->username));
//         // array_push($friendConnect, $b->username, $b->id);
//         // $friendConnect['username'] = $b->username;
//         // $friendConnect['id'] = $b->id;


//     }
// //    return sizeof($friendConnect);
// //    return $friendConnect;
//     $users = [];
//     $r_id = [];
//     for($i = 0; $i < sizeof($friendConnect); $i++)
//     {
//         foreach($friendConnect[$i] as $friends)
//         {
//            if($friends[0][$i]['first_user'] = $friends['id'])
//            {
//             $r_id = $friends['second_user'];
//            }else{
//             $r_id = $friends['first_user'];
//            }
//         }
//        $users[] = $friendConnect[$i];

//     }
//     return $r_id;
//     // foreach($friendConnect as $f)
//     // {
//     //     $user[] = $f[0];
//     // }
//     // return $users;
}
