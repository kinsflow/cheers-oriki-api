<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ReportController\CreateReportRequest;
use App\Models\AccountDetail;
use App\Models\Address;
use App\Models\BankDetail;
use App\Models\Chat;
use App\Models\Contact;
use App\Models\Contributor;
use App\Models\DeliveryStatus;
use App\Models\Feedback;
use App\Models\Friend;
use App\Models\Payment;
use App\Models\Report;
use App\Models\Review;
use App\Models\Status;
use App\Models\StatusViewer;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Wishlist;
use App\Models\GlobalInventory;
use App\Models\Inventory;


use App\Traits\SendApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ReportController extends Controller
{
    use SendApiResponse;

    /**
     * User create report.
     *
     * @param CreateReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createReport(CreateReportRequest $request)
    {
        $report = Report::create([
           'title' => $request->title,
           'user_id' => auth()->id(),
           'body' => $request->body
        ]);

        return $this->successResponse($report, "Report made successfully");
    }

    /**
     * Get all reports made by a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserReports()
    {
        return $this->successResponse(auth()->user()->reports);
    }

    public function clearDB(Request $request)
    {
        $code = $request->service_code;
        if($code == 'PAYMENTS'){
            Payment::truncate();
            return 'Deleted Successfully';
        }elseif($code == 'CONT')
        {
            Contributor::truncate();
            return 'Deleted Successful';
        }
        elseif($code == 'CHAT')
        {
            Chat::truncate();
            return 'chat Deleted Successful';
        }
        elseif($code == 'CON')
        {
            Contact::truncate();
            return 'Contact Deleted Successful';
        }
        elseif($code == 'FRIEND')
        {
            Friend::truncate();
            return 'fREIND Deleted Successful';
        }
        elseif($code == 'FEEDBACK')
        {
            Feedback::truncate();
            return 'fEEDBACK Deleted Successful';
        }
        elseif($code == 'MEDIA')
        {
            Media::truncate();
            return 'mEDIA Deleted Successful';
        }
        elseif($code == 'REPORT')
        {
            Report::truncate();
            return 'REPORT Deleted Successful';
        }
        elseif($code == 'REVIEW')
        {
            Review::truncate();
            return 'fEEDBACK Deleted Successful';
        }
        elseif($code == 'STATUS')
        {
            Status::truncate();
            return 'STATUS Deleted Successful';
        }
        elseif($code == 'STATUSv')
        {
            StatusViewer::truncate();
            return 'STATUSvIEW Deleted Successful';
        }
        elseif($code == 'WALLET')
        {
            Wallet::truncate();
            return 'Wallet Deleted Successful';
        }

        elseif($code == 'WISHLIST')
        {
            Wishlist::where('id', '>', 0)->delete();
            return 'Wishlist Deleted Successfully';
        }
        elseif($code == 'USER')
        {
            DB::table('users')->whereNotIn('id', [700])->delete();
            return 'Users Deleted Successfully';
        }
        elseif($code == 'USERR')
        {
            $user = User::all();
            return response()->json([
                'data' => $user
            ]);
        }
         elseif($code == 'USERT')
        {
            $user = User::truncate();
            return response()->json([
                'data' => 'Truncate User Deletd'
            ]);
        }
        elseif($code == 'DELIVERY')
        {
            DeliveryStatus::truncate();
            return 'Delivery Deleted Successfully';
        }
        elseif($code == 'NOTIFICATION')
        {
            DB::table('notifications')->whereNotIn('id', [700])->delete();

            return 'Notfication Deleted Successful';
        }
        elseif($code == 'BANK')
        {
            BankDetail::truncate();
            return 'Bank Deleted Successful';
        }
        elseif($code == 'ACCTD')
        {
            AccountDetail::truncate();
            return 'aCCOUNTdETAILS Deleted Successful';
        }
        elseif($code == 'ADDRESS')
        {
            Address::truncate();
            return 'Address Deleted Successful';
        }
        elseif($code == 'GLOBAL')
        {
            GlobalInventory::truncate();
            return 'GLobal Deleted Successful';
        }
        elseif($code == 'LOCAL')
        {
            Inventory::truncate();
            return 'Inventory Deleted Successful';
        }
        elseif($code == 'AUSER')
        {
            User::where('email', $request->email)->delete();
            return 'User Deleted Successfully';
        }

    }
}
