<?php

namespace App\Http\Controllers\Api\Home;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Home\StatusController\UploadStatusRequest;
use App\Models\Friend;
use App\Models\Status;
use App\Models\User;
use App\Traits\SendApiResponse;
use App\Traits\SendFCMNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusController extends Controller
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * Handle The uploads of status.
     *
     * @param UploadStatusRequest $request
     * @return JsonResponse
     */
    public function uploadStatus(UploadStatusRequest $request)
    {
        $user = Auth::user();

        $status = $user->statuses()->create([
            'text' => $request->text,
            'is_text' => $request->is_text,
            'color' => $request->color,
            'font' => $request->font
        ]);

        if ($request->status_media && !$request->is_text) {
            $status->addMediaFromRequest('status_media')->toMediaCollection(MediaCollections::STATUS);
        }

        // $this->sendMultipleDevice('Status Update', "{$user->last_name} {$user->first_name} just uploaded a new status", 'STATUS_UPDATE');
        $this->sendToFriendsDevice('Status Update', "{$user->last_name} {$user->first_name} just uploaded a new status", 'STATUS_UPDATE', Auth::id());


        return $this->successResponse($status, 'Status Uploaded successfully');
    }

    /**
     * Return all available status of a user.
     *
     * @return JsonResponse
     */
    public function fetchUserStatuses()
    {
        $user = Auth::user();

        $status = $user->statuses;

        return $this->successResponse($status, "My statuses fetched successfully.");
    }

    /**
     * Return all Status Of my connections on the platform.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchAllMyConnectionStatus(Request $request)
    {
        $orderBy = 'asc';

        if ($request->order_by_desc) {
            $orderBy = 'desc';
        }

        $authUserId = auth()->id();

        // fetch all my connection.
        $friendConnections = Friend::query()
            ->where('first_user', $authUserId)
            ->orWhere('second_user', $authUserId)
            ->get(['first_user', 'second_user']);

        $friendConnectionsArr = $friendConnections->toArray();

        $friendsIds = [];

        // Sort friend Ids.
        for ($i = 0; $i < sizeof($friendConnectionsArr); $i++) {
            if ($friendConnectionsArr[$i]['first_user'] !== \auth()->id()) {
                $friendsIds[] = $friendConnectionsArr[$i]['first_user'];
            } else {
                $friendsIds[] = $friendConnectionsArr[$i]['second_user'];
            }
        }

        $friendsStatuses = User::query()
            ->has('statuses')
            ->with('statuses')
            ->whereIn('id', $friendsIds)
            ->orderBy('created_at', $orderBy)
            ->paginate($request->per_page);

        return $this->successResponse($friendsStatuses, "Friends connection statuses fetched successfully.");
    }

    /**
     * Get the list of status of a particular user.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function fetchAUserStatus(User $user)
    {
        $status = $user->statuses;

        return $this->successResponse($status, "User statuses fetched successfully.");
    }
    /**
     * Return the viewers of a status.
     *
     * @param Status $status
     * @return JsonResponse
     */
    public function viewers(Status $status)
    {
        $viewers = [
            'viewers_count' => $status->viewers()->count(),
            'viewers' => $status->viewers()->with('viewer')->get()
        ];

        return $this->successResponse($viewers, "Status Viewers returned successfully");
    }

    /**
     * Keep record of viewers of a status.
     *
     * @param Status $status
     * @return JsonResponse
     */
    public function recordViewers(Status $status)
    {
        $user = Auth::user();

        if ($status->user == $user) {
            return $this->failureResponse("This status is for the currently authenticated user. hence, cannot be computed as part of view history");
        }

        $status->viewers()->upsert([
            'user_id' => $user->id,
            'status_id' => $status->id
        ], ['user_id', 'status_id']);

        return $this->successResponse([], "Status view history recorded successfully");
    }

    /**
     * Handle Collage data
     * @todo add contributors, messages, wishlist
     * @todo details in the current year of birthday
     * @todo when those module are available.
     *
     * @return JsonResponse
     */
    public function collageDetails()
    {
        $total_status = Auth::user()->statuses->count();

        return $this->successResponse([
            'total_status' => $total_status
        ], "Collage returned recorded successfully");
    }
    public function deleteStatus(Status $status)
    {
        $status->delete();
        return $this->successResponse([], 'Status has been deleted successfully');

    }
}
