<?php

namespace App\Http\Controllers\Api\WishList;

use App\Enums\MediaCollections;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Vendor\WishListController\ContributeRequest;
use App\Http\Requests\Api\Vendor\WishListController\StoreRequest;
use App\Models\Contributor;
use App\Models\GlobalInventory;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Wishlist;
use App\Notifications\Friends;
use App\Notifications\Wishlist as NotificationsWishlist;
use App\Traits\SendApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Auth;


class WishListController extends Controller
{
    use SendApiResponse;

    /**
     * Return all wishlist.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $wishlists = QueryBuilder::for(Wishlist::query())
            ->allowedFilters([
                'model_id',
                'model',
                'user_id'
            ])
            ->with('product')->orderBy('priority', 'asc')
            ->paginate($request->per_page);

        return $this->successResponse($wishlists, 'Wishlist Returned Successfully');
    }

    /**
     * Create WishList.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {

        switch ($request->model) {
            case 'global_inventory':
                $inventoryType = GlobalInventory::class;
                break;
            default:
                $inventoryType = Inventory::class;
        }
        if(Wishlist::where('user_id', auth()->id())->where('model_id', $request->model_id)->exists())
        {
            return $this->failureResponse('Item has been added to your wishlist already');
        }
        if ($inventoryType == GlobalInventory::class) {
            $globalInventory = GlobalInventory::create([
                'title' => $request->title,
                'name' => 'Global Inventory',
                'description' => $request->description,
                'price' => $request->price
            ]);

            $globalInventory->addMediaFromRequest('image')->toMediaCollection(MediaCollections::GLOBALINVENTORYIMAGE);
            // $globalInventory->addMediaFromRequest('image')->toMediaCollection(MediaCollections::CHAT);

        }

        $wishlist = Wishlist::create([
            'model' => $inventoryType,
            'user_id' => auth()->id(),
            'model_id' => $request->model_id ?: $globalInventory->id,
            'quantity' => $request->quantity,
        ]);

        return $this->successResponse($wishlist, 'Wishlist Added Successfully');
    }

    /**
     * Delete Content.
     *
     * @param Wishlist $wishlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Wishlist $wishlist)
    {
        $wishlist->delete();

        return $this->successResponse($wishlist, 'Wishlist Deleted Successfully', Response::HTTP_NO_CONTENT);
    }

    /**
     * Contribute to a wishlist Item.
     *
     * @param ContributeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contribute(ContributeRequest $request)
    {
        $userId = auth()->id();

        $wishList = Wishlist::find($request->wishlist_id);

        $RecipientUser = User::find($wishList->user_id);
        $contributingUser = auth()->user();

        $wallet = Wallet::whereUserId($userId)->first();
        if (!$wallet) {
            return $this->failureResponse('Contributor Has No Functional Wallet');
        }

        $accountBalance = $wallet->balance;

        $checkIfUserHaveSufficientBalance = $accountBalance >= $request->amount;

        if (!$checkIfUserHaveSufficientBalance) {
            return $this->failureResponse('Insufficient Balance');
        }

        DB::transaction(function () use ($request, $wallet, $wishList) {
            // Deduct money contributed
            $wallet->balance -= $request->amount;

            $wallet->save();

            // Record Contibution history
            $contributor = new Contributor();
            $contributor->wishlist()->associate($wishList);
            $contributor->user()->associate(auth()->user());
            $contributor->amount_contributed = $request->amount;

            $contributor->save();

            // Update amount realized on item.
            $wishList->amount_realized += $request->amount;

            $wishList->save();
        });
        $message = 'Your Wishlist Item Has been funded with a sum of ₦' .$request->amount.' by '.$contributingUser->username;
        $RecipientUser->notify(new NotificationsWishlist($message));
        return $this->successResponse([], 'Contribution made Successfully');
    }

    /**
     * Fetch Contributor of a particular wishlist item.
     *
     * @param Request $request
     * @param Wishlist $wishlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchWishListItemContributors(Request $request, Wishlist $wishlist)
    {
        $contributors = $wishlist->contributors()->paginate($request->per_page);

        return $this->successResponse($contributors);
    }

    public function DeleteWishListItemContributors(Request $request, Wishlist $wishlist)
    {
        $contributors = $wishlist->contributors()->delete();

        return $this->successResponse([], 'Deleted successfully');
    }

    /*
    *Fetch all users wishlist alongside thier Contributors
    */
    public function fecthAllContributors(Request $request)
    {
        $allContributors = QueryBuilder::for(Wishlist::query())
        ->allowedFilters([
            'model_id',
            'model',
            'user_id'
        ])
        ->with('contributors')
        ->paginate($request->per_page);
        return $this->successResponse($allContributors);
    }


    /**
     * Re-order the wishlist in order of their priority
     */
    public function setWishlistPriority(Request $request)
    {
        $userId = Auth()->id();
        $wishlists = $request->toArray();
        foreach($wishlists as $wishlist)
        {
            Wishlist::whereId($wishlist['id'])->where('user_id', $userId)->update([
                'priority' => $wishlist['priority']
            ]);
        }
        return $this->successResponse('Wishlist Priority has been updated successfully');
    }
}
