<?php

namespace App\Http\Controllers\Api\WishList;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\WishList\DeliveryStatusController\SetStatusRequest;
use App\Models\Wishlist;
use App\Traits\SendApiResponse;

class DeliveryStatusController extends Controller
{
    use SendApiResponse;

    /**
     * Set The Delivery Status Of A Wishlist.
     *
     * @param SetStatusRequest $request
     * @param Wishlist $wishlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStatus(SetStatusRequest $request, Wishlist $wishlist)
    {
        $deliveryStatus = $wishlist->deliveryStatus()->firstOrCreate(
            ['wishlist_id' => $wishlist->id],
            ['status' => $request->status, 'location' => $request->location]);

        $deliveryStatus->save();

        return $this->successResponse($deliveryStatus, 'Wishlist delivery status updated successfully');
    }

    /**
     * Retrieve The Delivery Status Of A Wishlist.
     *
     * @param Wishlist $wishlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveryStatus(Wishlist $wishlist)
    {
        $wishlistDeliveryStatus = $wishlist->deliveryStatus()->firstOr(function (){
            return 'Order not necessarily approved yet';
        });

        return $this->successResponse($wishlistDeliveryStatus);
    }
}
