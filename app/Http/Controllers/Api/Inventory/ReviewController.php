<?php

namespace App\Http\Controllers\Api\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Inventory\ReviewController\GetAnInventoryReviewRequest;
use App\Http\Requests\Api\Inventory\ReviewController\RateAnInventoryRequest;
use App\Models\GlobalInventory;
use App\Models\Inventory;
use App\Models\Review;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    use SendApiResponse;

    /**
     * Rate an Inventory.
     *
     * @param RateAnInventoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rateAnInventory(RateAnInventoryRequest $request, $inventory)
    {
        $endpointNamespace = explode('/', $request->url())[5];

        switch ($endpointNamespace) {
            case 'global':
                $inventoryType = GlobalInventory::class;
                break;
            default:
                $inventoryType = Inventory::class;
        }

        $review = Review::create([
            'user_id' => auth()->id(),
            'model' => $inventoryType,
            'model_id' => $inventory,
            'comment' => $request->comment,
            'rating' => $request->rating
        ]);

        return $this->successResponse($review, 'Review created successfully');
    }

    /**
     * Get Review of an inventory
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnInventoryReview(GetAnInventoryReviewRequest $request, $inventory)
    {
        switch ($request->model) {
            case 'global_inventory':
                $inventoryType = GlobalInventory::class;
                break;
            default:
                $inventoryType = Inventory::class;
        }

        $reviews = Review::query()
            ->whereModel($inventoryType)
            ->whereModelId($inventory)
            ->paginate($request->per_page);

        return $this->successResponse($reviews, 'Review returned successfully');
    }
}
