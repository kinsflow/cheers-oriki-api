<?php

namespace App\Http\Controllers\Api\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Vendor\InventoryController\IndexRequest;
use App\Models\Inventory;
use App\Traits\SendApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class LocalInventoryController extends Controller
{
    use SendApiResponse;

    /**
     * Return Inventories and filter by vendor_id and category_id.
     *
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request): JsonResponse
    {
        $inventories = QueryBuilder::for(Inventory::query())
            ->allowedFilters([
                'category_id',
                'vendor_id'
            ])->with('vendor')
            ->orderBy('id', 'desc')
            ->paginate($request->per_page);

        return $this->successResponse($inventories, 'Inventories Returned Successfully');
    }

    /**
     *
     * return a single inventory resource.
     *
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function show(Inventory $inventory)
    {
        return $this->successResponse($inventory->load('reviews'), 'Inventory Returned Successfully');
    }
    public function getVendorProduct($vendor_id)
    {
        $vendor_products = Inventory::where('vendor_id', $vendor_id)->get();
        return $this->successResponse($vendor_products, 'Vendor Products Returned Successfully');
    }
}
