<?php

namespace App\Http\Controllers\Api\Inventory;

use App\Http\Controllers\Controller;
use App\Models\GlobalVendor;
use App\Traits\SendApiResponse;
use Illuminate\Http\Request;

class GlobalInventoryController extends Controller
{
    use SendApiResponse;

    public function vendor(Request $request)
    {
        $globalVendors = GlobalVendor::paginate($request->per_page);

        return $this->successResponse($globalVendors);
    }
}
