<?php

namespace App\Http\Controllers\Services;

use App\Enums\FlutterwaveCountries;
use App\Enums\FlutterwaveCurrencies;
use App\Enums\PaymentActionType;
use App\Http\Requests\Services\Flutterwave\GetBanksRequest;
use App\Http\Requests\Services\Flutterwave\ResolveAccountNameRequest;
use App\Models\Payment;
use App\Models\Wallet;
use App\Traits\SendApiResponse;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Traits\SendFCMNotification;


class Flutterwave
{
    use SendApiResponse;
    use SendFCMNotification;

    /**
     * Connect to flutterwave payment service.
     *
     * @return Client
     */
    protected function flutterWave()
    {
        return new Client([
            'base_uri' => config('app.flutterwave_base_url'),

            'headers' => [
                'Authorization' => 'Bearer ' . config('app.flutterwave_secret_key'),
            ]
        ]);
    }

    /**
     * Send Payment Detail to flutterwave to initialize payment.
     *
     * @param Model $payment
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendPaymentToFlutterWave(Model $payment)
    {
        $sendToFlutterWave = $this->flutterWave()->post('v3/payments',
            [
                'json' => [
                    'tx_ref' => $payment->id,
                    'amount' => $payment->amount,
                    'currency' => 'NGN',
                    'payment_options' => 'card',

                    'customer' => [
                        'email' => $payment->user->email,
                        'phonenumber' => $payment->user->phone,
                        'name' => $payment->user->username,
                    ],
                    'meta' => [
                        'user_id' => $payment->user->id,
                    ],
                    'customizations' => [
                        'title' => 'Cheers App Wallet Funding',
                        'description' => 'You are just about funding your Cheers wallet',
                        'logo' => asset('images/cheers-logo.png')
                    ],
                    'redirect_url' => config('app.url')
                ]
            ]
        );

        return $sendToFlutterWave->getBody();
    }

    /**
     * Receive payment from flutterwave.
     *
     * @param Model $payment
     * @param Request $request
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function receivePaymentFromFlutterwave(Request $request)
    {
        $receiveFromFlutterwave = $this->flutterWave()->post('v3/transfers',
            [
                'json' => [
                    'account_bank' => $request->account_bank,
                    'account_number' => $request->account_number,
                    'amount' => $request->amount,
                    'narration' => "Withdrawal From " . config('app.name'),
                    'currency' => $request->currency,
                    'reference' => Str::uuid(),
                    'callback_url' => config('app.url'),
                    'debit_currency' => 'NGN',
                ]
            ]
        );

        return $receiveFromFlutterwave->getBody();
    }

    /**
     * Create Payment history.
     *
     * @param Wallet $wallet
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createPayment(Wallet $wallet, Request $request)
    {
        return $wallet->payments()->create([
            'amount' => $request->amount,
            'wallet_id' => $wallet->id,
            'user_id' => Auth::id(),
            'transaction_date' => Carbon::parse($request->transaction_date),
            'transaction_action_type' => PaymentActionType::FUND_WALLET_ACTION
        ]);
    }

    /**
     * Create Withdrawal record on Payment model
     * when a user withdraw from their wallet.
     *
     * @param Wallet $wallet
     * @param Request $request
     * @param $withrawalResponse
     * @return Model
     */
    public function createWithrawalRecord(Wallet $wallet, Request $request, $withrawalResponse)
    {
        return $wallet->payments()->create([
            'amount' => $request->amount,
            'wallet_id' => $wallet->id,
            'user_id' => Auth::id(),
            'transaction_action_type' => PaymentActionType::WITHDRAW_WALLET_ACTION,
            'paid_at' => Carbon::parse($withrawalResponse->created_at),
            'account_number' => $withrawalResponse->account_number,
            'account_bank' => $withrawalResponse->bank_code,
            'status' => $withrawalResponse->status,
            'reference' => $withrawalResponse->reference,
            'transaction_date' => $request->transaction_date,
        ]);
    }

    /**
     * Account Verification | Resolve Account Name
     * @param ResolveAccountNameRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resolveAccountName(ResolveAccountNameRequest $request)
    {
        try {
            $account_name = json_decode($this->flutterWave()->post('v3/accounts/resolve', [
                'json' => [
                    'account_number' => $request->account_number,
                    'account_bank' => $request->account_bank
                ]
            ])->getBody()->getContents())->data;

            return $this->successResponse($account_name, "Account Verified / Resolved Successfully");
        }catch (\Exception $exception){
            return $this->failureResponse("Incorrect Account Details", $exception->getCode());
        }
    }

    /**
     * Webhook handler from flutterwave for successful payment.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\never
     */
    public function updateSuccessfulTransaction(Request $request)
    {
        if ($request->header('verif-hash') !== config('app.flutterwave_secret_hash')) {
            return abort(403);
        }

        $webhookBody = json_decode($request->getContent());

        if ($webhookBody->status == 'successful') {
            $payment = Payment::where('paid_at', null)->find($webhookBody->txRef);

            if (!$payment) {
                return response()->json([
                    'message' => 'Payment already verified.',
                ]);
            }

            $payment->forceFill([
                'payment_type' => $webhookBody->{'event.type'},
                'status' => $webhookBody->status,
                'paid_at' => Carbon::parse($webhookBody->createdAt),
                'reference' => $webhookBody->orderRef
            ])->save();

            $wallet = Wallet::find($payment->wallet_id);

            $wallet->user_id = $payment->user_id;
            $wallet->balance += $payment->amount;
            $wallet->last_funded = Carbon::parse($webhookBody->createdAt);
            $wallet->save();
            $msg = 'Your wallet has been funded with ₦'.$payment->amount.'';
            $this->sendSingleDevice('Wallet Funding', $msg, 'WALLET_FUNDING', $payment->user_id);
            return response()->json([
                'data' => null,
            ]);
        }
    }

    /**
     * Get the list of all banks in a country.
     *
     * @param GetBanksRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBanks(GetBanksRequest $request)
    {
        $banks = $this->flutterWave()->get("/v3/banks/{$request->country_abbr}");

        return $this->successResponse(json_decode($banks->getBody()->getContents())->data);
    }

    /**
     * Get list countries available for
     * transfers on flutterwave.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountries()
    {
        $countries = [
            [
                "country" => "Nigeria",
                "abbr" => FlutterwaveCountries::NIGERIA,
                "currency" => FlutterwaveCurrencies::NIGERIA
            ],
            [
                "country" => "Ghana",
                "abbr" => FlutterwaveCountries::GHANA,
                "currency" => FlutterwaveCurrencies::GHANA
            ],
            [
                "country" => "Kenya",
                "abbr" => FlutterwaveCountries::KENYA,
                "currency" => FlutterwaveCurrencies::KENYA
            ],
            [
                "country" => "Uganda",
                "abbr" => FlutterwaveCountries::UGANDA,
                "currency" => FlutterwaveCurrencies::UGANDA
            ],
            [
                "country" => "South Africa",
                "abbr" => FlutterwaveCountries::SOUTH_AFRICA,
                "currency" => FlutterwaveCurrencies::SOUTH_AFRICA
            ],
            [
                "country" => "Tanzania",
                "abbr" => FlutterwaveCountries::TANZANIA,
                "currency" => FlutterwaveCurrencies::TANZANIA
            ],
        ];

        return $this->successResponse($countries);
    }

    /**
     * Return a user payment history.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchTransactionHistory(Request $request)
    {
        $paymentHistory = Payment::query()
            ->select(['amount', 'paid_at', 'user_id', 'transaction_action_type', 'payment_type', 'transaction_date', 'status'])
            ->whereUserId(auth()->id())->whereNotNull('paid_at')->paginate($request->per_page);

        return $this->successResponse($paymentHistory);
    }
}
