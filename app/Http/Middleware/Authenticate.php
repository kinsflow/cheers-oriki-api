<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Authenticate extends Middleware
{
    /**
     * Exclude these routes from authentication check.
     *
     * @var array
     */
    protected $except = [
        'api/logout',
        'api/refresh',
    ];

    /**
     * Ensure the user is authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        try {
                foreach ($this->except as $excluded_route) {
                    if ($request->path() === $excluded_route)
                    {
                        \Log::debug("Skipping $excluded_route from auth check...");
                        return $next($request);
                    }
                }
                \Log::debug('Authenticating... '. $request->url());
                JWTAuth::parseToken()->authenticate();

                return $next($request);
            }
            catch (TokenExpiredException $e) {
                \Log::debug('token has expired');
                try {
                    $customClaims = [];
                    $refreshedToken = JWTAuth::claims($customClaims)
                        ->refresh(JWTAuth::getToken());
                }
                catch (TokenExpiredException $e) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'token has expired',
                        'data' => null,
                    ], 401);
                }

                return response()->json([
                    'status' => 'failed',
                    'message' => 'token has expired and refreshed',
                    'data' => [
                        'token' => $refreshedToken,
                    ],
                ], 401);
            }
            catch (TokenInvalidException $e) {

                \Log::debug('token invalid');
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                    'data' => null
                ], 401);
            }
            catch (TokenBlacklistedException $e) {
                \Log::debug('token blacklisted');
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                    'data' => null
                ], 401);
            }
            catch (JWTException $e) {
                \Log::debug('token is absent');
                return response()->json([
                    'status' => 'failed',
                    'message' => 'token absent',
                    'data' => null
                ], 401);

            }

    }
}
