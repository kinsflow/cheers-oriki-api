<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $fillable = ["balance", "last_funded", "last_withdrawn", "user_id"];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
