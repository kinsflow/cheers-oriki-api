<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryStatus extends Model
{
    use HasFactory;

    protected $fillable = ['wishlist_id', 'status', 'location'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class);//->where('model', Inventory::class)->first();
    }
}
