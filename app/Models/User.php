<?php

namespace App\Models;


use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject, HasMedia
{
    use HasFactory;
    use Notifiable;
    use InteractsWithMedia;
    use SoftDeletes;

    protected $rememberTokenName = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'phone',
        'date_of_birth',
        'password',
        'avatar',
        'email_verified_at',
        'remember_token',
        'device_token'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'media',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_of_birth' => 'datetime:d/m/Y',
    ];

    /**
     * This should append the following on call of
     * the user model
     * @var string[]
     */
    protected $appends = ['address'];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::PROFILEPICTURE)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes(MediaTypes::IMAGES)
            ->singleFile();
    }

    /**
     * Get a user's contacts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    /**
     * Get full URL for the user's profile picture.
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::PROFILEPICTURE);
    }

    public function getFirstName($value)
    {
        return ucfirst($value);
    }

    public function getLastName($value)
    {
        return ucfirst($value);
    }

    public function getFullName()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name) ?? null;
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function myAddress()
    {
        return $this->hasOne(Address::class);
    }

    public function getAddressAttribute()
    {
        return $this->myAddress;
    }

    public function closeAccount()
    {
        $this->is_closed = true;
        $this->save();
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class);
    }

    public function statuses()
    {
        return $this->hasMany(Status::class)->whereYear('created_at', now()->year);
    }

    public function bankDetails()
    {
        return $this->hasOne(BankDetail::class);
    }
    public function contributors()
    {
        return $this->hasMany(Contributor::class);
    }
    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }
}
