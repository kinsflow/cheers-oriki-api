<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Friend extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['first_user', 'second_user', 'acted_user', 'status'];

    /**
     * First user on the relationship.
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firstUser()
    {
        return $this->belongsTo(User::class, 'first_user');
    }

    /**
     * Second user on the relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondUser()
    {
        return $this->belongsTo(User::class, 'second_user');
    }

    /**
     * Return the chat history of a friend connection.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chats()
    {
        return $this->hasMany(Chat::class, 'friend_connection_id');
    }
}
