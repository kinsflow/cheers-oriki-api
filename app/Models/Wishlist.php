<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    use HasFactory;

    protected $fillable = ['model', 'quantity', 'amount_realized', 'model_id', 'user_id', 'priority'];

    public function contributors()
    {
        return $this->hasMany(Contributor::class);
    }

    public function deliveryStatus()
    {
        return $this->hasOne(DeliveryStatus::class);
    }

    public function product()
    {
        return $this->morphTo('product', 'model', 'model_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->select(['id', 'first_name', 'last_name', 'phone', 'email']);
    }

}
