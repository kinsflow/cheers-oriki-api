<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Status extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = ['user_id', 'is_text', 'text', 'color', 'font'];

    protected $appends = ['status_media'];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::STATUS)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes([...MediaTypes::VIDEOS, ...MediaTypes::IMAGES])
            ->singleFile();
    }

    /**
     * Get full URL for the status' media.
     *
     * @return string
     */
    public function getStatusMediaAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::STATUS);
    }

    /**
     * The owner of a status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function viewers()
    {
        return $this->hasMany(StatusViewer::class);
    }
}
