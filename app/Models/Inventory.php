<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Inventory extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use SoftDeletes;

    protected $with = ['media', 'category'];

    protected $fillable = ['title', 'description', 'vendor_id', 'category_id', 'price', 'stock', 'active', 'sku'];

    protected $appends = ['average_rating'];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::INVENTORYGALLERY)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes([...MediaTypes::VIDEOS, ...MediaTypes::IMAGES]);
    }

    /**
     * Inventory Belongs to A Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Inventory Belongs to A Vendor.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    /**
     * Get Global inventory review
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable', 'model', 'model_id');
    }

    /**
     * Get the average rating on the inventory's reviews.
     *
     * @return int
     */
    public function getAverageRatingAttribute()
    {
        $avgRating =  $this->reviews()->avg('rating') ?? 0;
        return floatval($avgRating);
    }
    public function getWishlist()
    {
        return $this->belongsTo(Wishlist::class, 'model_id');
    }
}
