<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class GlobalVendor extends Model implements HasMedia
{
    use HasFactory;
    use SoftDeletes;
    use InteractsWithMedia;

    protected $fillable = ['name', 'url'];
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::GLOBALVENDORSLOGO)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes([ ...MediaTypes::IMAGES])
            ->singleFile();
    }

    /**
     * Get full URL for the global vendor's logo.
     *
     * @return string
     */
    public function getChatMediaAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::GLOBALVENDORSLOGO);
    }

    /**
     * Get Global inventory review
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable', 'model', 'model_id');
    }
}
