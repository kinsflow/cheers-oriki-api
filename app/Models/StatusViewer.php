<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusViewer extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'status_id'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function viewer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
