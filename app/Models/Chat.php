<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Chat extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = ['friend_id', 'sender_id', 'is_text', 'text', 'recipient_id'];

    protected $appends = ['chat_media'];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::CHAT)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes([...MediaTypes::VIDEOS, ...MediaTypes::IMAGES, ...MediaTypes::AUDIOS])
            ->singleFile();
    }

    /**
     * Get full URL for the status' media.
     *
     * @return string
     */
    public function getChatMediaAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::CHAT);
    }

    /**
     * Get the friend connection that a particular chat belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function friendConnection()
    {
        return $this->belongsTo(Friend::class, 'friend_connection_id');
    }

    /**
     * Return the sender of a message in a chat.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }
    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }
}
