<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class GlobalInventory extends Model implements HasMedia
{

    use HasFactory;
    use SoftDeletes;
    use InteractsWithMedia;

    protected $fillable = ['name', 'title', 'description', 'price'];

    // protected $with = ['inventory_image'];

    protected $appends = ['average_rating', 'inventory_image'];


    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::GLOBALINVENTORYIMAGE)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes([...MediaTypes::IMAGES])
            ->singleFile();
    }

    /**
     * Get full URL for the status' media.
     *
     * @return string
     */
    public function getInventoryImageAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::GLOBALINVENTORYIMAGE);
    }
    // public function inventory_image(): BelongsTo {

    //     return $this->belongsTo(User::class, 'user_id');
    // }

    /**
     * Get Global inventory review
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable', 'model', 'model_id');
    }

    /**
     * Get the average rating on the inventory's reviews.
     *
     * @return int
     */
    public function getAverageRatingAttribute()
    {
        return $this->reviews()->avg('rating') ?? 0;
    }
}
