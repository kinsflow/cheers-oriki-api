<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contributor extends Model
{
    use HasFactory;

    protected $with = ['user'];

    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
