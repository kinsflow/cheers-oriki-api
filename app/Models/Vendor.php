<?php

namespace App\Models;

use App\Enums\MediaCollections;
use App\Enums\MediaTypes;
use App\Notifications\VerifyVendorEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Vendor extends Authenticatable implements HasMedia, JWTSubject, MustVerifyEmail
{
    use HasFactory;
    use InteractsWithMedia;
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'vendor_name',
        'email',
        'username',
        'phone',
        'password',
        'email_verified_at',
    ];

    protected $appends = ['profile_picture'];

    protected $hidden = ['password'];

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollections::VENDORPROFILEPICTURE)
            ->useFallbackUrl(url('/images/profile-picture.png'))
            ->acceptsMimeTypes(MediaTypes::IMAGES)
            ->singleFile();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get full URL for the status' media.
     *
     * @return string
     */
    public function getProfilePictureAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollections::VENDORPROFILEPICTURE);
    }

    public function getJWTCustomClaims()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'vendor_name' => $this->vendor_name,
            'phone' => $this->phone,
            'username' => $this->username,
        ];
    }

    public function hasVerifiedEmail()
    {
        // TODO: Implement hasVerifiedEmail() method.
    }

    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyVendorEmail());
    }

    public function getEmailForVerification()
    {
        // TODO: Implement getEmailForVerification() method.
    }
}
