<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['title', 'body', 'user_id'];

    protected $table = 'feedbacks';

    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
