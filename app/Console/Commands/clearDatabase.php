<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class clearDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears the database after a specified period of time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $calcTime = Carbon::now()->subHours(23);
    
        // // $calcTime = Carbon::now()->subMinutes(3);
        // $status = Status::where('created_at', '<', $calcTime)->get();
        // foreach($status as $s)
        // {
        //     $s->delete();
        // }
    }
}
