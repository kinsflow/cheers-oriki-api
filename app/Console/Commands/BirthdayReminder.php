<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Traits\SendFCMNotification;

class BirthdayReminder extends Command
{
    use SendFCMNotification;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a birthday reminder to friends';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bu = User::whereDay('date_of_birth', Carbon::now())->whereMonth('date_of_birth', Carbon::now())->get();

        foreach ($bu as $b)
        {
            $msg = $b->first_name .' ' .$b->last_name.' is celebrating today, wish them well';
            $this->sendToFriendsDevice('Today\'s Birthday', $msg, 'BIRTHDAY', $b->id);
        }
    }
}
