<?php

namespace App\Console\Commands;

use App\Models\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class DeleteStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete A user Status after 24Hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $calcTime = Carbon::now()->subHours(23);
        // $calcTime = Carbon::now()->subMinutes(3);
        $status = Status::where('created_at', '<', $calcTime)->get();
        foreach($status as $s)
        {
            $s->delete();
        }
    }
}
