<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Traits\SendFCMNotification;

class EbirthdayReminder extends Command
{
    use SendFCMNotification;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ebirthday:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $bu = User::whereDay('date_of_birth', Carbon::now())->whereMonth('date_of_birth', Carbon::now())->get();

        foreach ($bu as $b)
        {

            $msg = 'In case you missed it ' .$b->first_name .' ' .$b->last_name.' is inviting you to celebrate with them';
            $this->sendToFriendsDevice('Today\'s Birthday', $msg, 'BIRTHDAY', $b->id);
        }
    }
}
