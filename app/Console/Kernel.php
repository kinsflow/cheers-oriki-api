<?php

namespace App\Console;

use App\Console\Commands\AbirthdayReminder;
use App\Console\Commands\BirthdayReminder;
use App\Console\Commands\DeleteStatus;
use App\Console\Commands\EbirthdayReminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        DeleteStatus::class,
        BirthdayReminder::class,
        AbirthdayReminder::class,
        EbirthdayReminder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('delete:status')->everyMinute();
        $schedule->command('birthday:reminder')->dailyAt('06:00');
        $schedule->command('abirthday:reminder')->dailyAt('12:00');
        $schedule->command('ebirthday:reminder')->dailyAt('18:00');
        $schedule->command('model:prune')->dailyAt('07:00');




    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
