<?php

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;



/** Sent OTP via smartsmssolution API */
function sendOtpSMS(array $data) {
    $body = [
        'token' => env("SMART_SMS_TOKEN"),
        'sender' => env("SMART_SMS_SENDER_ID"),
        'to' => $data['recipients'],
        'message' => $data['message'],
        'type' => 0,
        'routing' => 2
    ];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => env("SMART_SMS_BASE_URL") . 'sms/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('token' => env("SMART_SMS_TOKEN"), 'sender' =>env("SMART_SMS_SENDER_ID"), 'to' => $data['recipients'], 'message' => $data['message'], 'type' => '0', 'routing' => '3')
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $res = json_decode($response);
    if ($res->code == 1000 && $res->comment == 'Completed Successfully') {
        return True;
    }
    else {
        return False;
    }

}


/** Send POST CURL request */
function curl($url, $post = "") {
    $curl = curl_init();
    $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
    curl_setopt($curl, CURLOPT_URL, $url);
    //The URL to fetch. This can also be set when initializing a session with curl_init().
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    //The number of seconds to wait while trying to connect.
    if ($post != "") {
        curl_setopt($curl, CURLOPT_POST, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    }
    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
    //The contents of the "User-Agent: " header to be used in a HTTP request.
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    //To follow any "Location: " header that the server sends as part of the HTTP header.
    curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
    //To automatically set the Referer: field in requests where it follows a Location: redirect.
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    //The maximum number of seconds to allow cURL functions to execute.
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    //To stop cURL from verifying the peer's certificate.
    $contents = curl_exec($curl);
    curl_close($curl);
    return $contents;
}

