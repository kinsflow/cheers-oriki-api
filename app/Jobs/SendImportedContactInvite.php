<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendImportedContactInvite as SendImportedContactInviteNotification;

class SendImportedContactInvite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $contactEmails;

    /**
     * Create a new job instance.
     *
     * @param array $contactEmails
     */
    public function __construct(array $contactEmails)
    {
        $this->contactEmails = $contactEmails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
           Notification::route('mail', $this->contactEmails)->notify(new SendImportedContactInviteNotification());
    }
}
