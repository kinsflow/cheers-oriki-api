<?php
namespace App\Repositories;
use App\Repositories\RepositoryInterfaces\UploadImageInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;



class UploadImageRepository implements UploadImageInterface {
    /**
     * Uploads a new image.
     *
     * @param mixed $request
     * @author Niklas Fandrich
     * @return mixed
     */
    public function upload(Request $request)
    {
        return $this->storeImage($request);
    }


    /**
     * Prepares a image for storing.
     *
     * @param mixed $request
     * @author Niklas Fandrich
     * @return bool
     */
    public function storeImage($request)
    {
        // Get file from request
        $file = $request->file('avatar');

        // Get filename with extension
        $filenameWithExt = $file->getClientOriginalName();

        // Get file path
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);


        // Remove unwanted characters
        $filename = preg_replace("/[^A-Za-z0-9 ]/", '', $filename);
        $filename = preg_replace("/\s+/", '-', $filename);

        // Get the original image extension
        $extension = $file->getClientOriginalExtension();

        // Create unique file name
        $fileNameToStore = $request->username ? $request->username .  '.' . $extension : auth()->user()->username . '.' . $extension;

        // Refer image to method resizeImage
        $save = $this->resizeImage($file, $fileNameToStore);

        return $save;
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file, $fileNameToStore)
    {
        //Resize image
        $resize = Image::make($file)->resize(128, 128, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');


        // Create hash value
        $hash = md5($resize->__toString());

        // Prepare qualified image name
        $image = $hash . "jpg";

        // Put image to storage
        $save = Storage::disk('s3')->put("public/avatar/{$fileNameToStore}", $resize->__toString(), 'public');
        if ($save) {
            return "public/avatar/{$fileNameToStore}";
        }
        return false;
    }
}
