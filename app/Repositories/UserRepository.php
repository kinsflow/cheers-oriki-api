<?php

namespace App\Repositories;
use App\Repositories\RepositoryInterfaces\UserInterface;
use App\Models\User;


class UserRepository implements UserInterface {

    protected $model;

    public function __construct(User $model) {
        return $this->model = $model;
    }


    public function all(){
        return $this->model->all();
    }


    public function getById($id) {
        return $this->model->findById($id);
    }



    public function create(array $data) {
        return $this->model->create($data);
    }


    public function show($id) {
        return $this->model->findOrFail($id);
    }


    public function delete($id) {
        return $this->model->destroy($id);
    }


    public function update($id, array $data) {
        $records = $this->model->findOrFail($id);
        return $records->update($data);
    }

    public function pagenate($number) {
        return $this->model->pagenate($number);
    }


}
