<?php

namespace App\Repositories\RepositoryInterfaces;

use Illuminate\Http\Request;


interface UploadImageInterface {


    public function upload(Request $request);

}
