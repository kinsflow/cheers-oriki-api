<?php
namespace App\Repositories\RepositoryInterfaces;

interface UserInterface {

    public function all();

    public function create(array $data);

    public function getById($id);

    public function update($id, array $data);

    public function delete($id);

    public function show($id);

}
