<?php

namespace App\Services;
use Illuminate\Http\Request;
use Google_Client, Google_Service_PeopleService;


class ContactService {

    /* Return Google Client */
    public function generateClient($access_token) {
        $google_client_id = env("GOOGLE_CLIENT_ID");
        $google_client_secret = env("GOOGLE_CLIENT_SECRET");
        $google_redirect_uri =  env('GOOGLE_LOGIN_REDIRECT');
        $scopes = [
                    Google_Service_PeopleService::USER_ADDRESSES_READ,
                    Google_Service_PeopleService::USER_BIRTHDAY_READ,
                    Google_Service_PeopleService::USER_EMAILS_READ,
                    Google_Service_PeopleService::USER_PHONENUMBERS_READ,
                ];
        // client objects
        $client = new Google_Client();
        $client->setApplicationName('People API PHP Quickstart');
        $client->setScopes($scopes);
        $client->setClientid($google_client_id);
        $client->setClientSecret($google_client_secret);
        $client->setRedirectUri($google_redirect_uri);
        $client->setAccessType('online');
        $client->setPrompt('select_account consent');
        $client->setAccessToken($access_token);
        return $client;
    }

    /* Return OAUTH URL */

    public function generateOauthUrl() {
        $google_client_id = env("GOOGLE_CLIENT_ID");
        $google_client_secret = env("GOOGLE_CLIENT_SECRET");
        $google_redirect_uri =  env('GOOGLE_LOGIN_REDIRECT');
        $client = new Google_Client();
        $client->setApplicationName('People API PHP Quickstart');
        $client->setScopes(Google_Service_PeopleService::CONTACTS_READONLY);
        $client->setClientid($google_client_id);
        $client->setClientSecret($google_client_secret);
        $client->setRedirectUri($google_redirect_uri);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $authUrl = filter_var($client->createAuthUrl(), FILTER_SANITIZE_URL);
        return $authUrl;
    }

    public function generateAccessToken($code) {
        $google_client_id = env("GOOGLE_CLIENT_ID");
        $google_client_secret = env("GOOGLE_CLIENT_SECRET");
        $google_redirect_uri =  env('GOOGLE_LOGIN_REDIRECT');
        $auth_code = $code;
        $fields=array(
            'code'=>  urlencode($auth_code),
            'client_id'=>  urlencode($google_client_id),
            'client_secret'=>  urlencode($google_client_secret),
            'redirect_uri'=>  urlencode($google_redirect_uri),
            'grant_type'=>  urlencode('authorization_code')
        );
        $post = '';
        foreach($fields as $key=>$value)
        {
            $post .= $key.'='.$value.'&';
        }	
        $post = rtrim($post,'&');
        $accesstoken ="";
        $result = curl('https://accounts.google.com/o/oauth2/token',$post);
        $response =  json_decode($result);
        $accesstoken = $response->access_token;
        return $accesstoken;
    }
}