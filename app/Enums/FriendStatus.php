<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class FriendStatus extends Enum
{
    public const PENDING = 'pending';
    public const CONFIRMED = 'confirmed';
    public const BLOCKED = 'blocked';
    public const UNBLOCKED = 'unblocked';
    public const DECLINED = 'declined';
}
