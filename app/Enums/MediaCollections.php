<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class MediaCollections
 * @package App\Enums
 */
final class MediaCollections extends Enum
{
    public const PROFILEPICTURE = 'profile_picture';
    public const VENDORPROFILEPICTURE = 'vendor_profile_picture';
    public const STATUS = 'status_media';
    public const LOGO = 'logo';
    public const CHAT = 'chat';
    public const INVENTORYGALLERY = 'inventory_gallery';
    public const GLOBALVENDORSLOGO = 'global_vendors_logo';
    public const GLOBALINVENTORYIMAGE = 'global_inventory_image';
}
