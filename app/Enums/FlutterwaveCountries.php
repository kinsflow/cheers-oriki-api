<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class FlutterwaveCountries
 * @package App\Enums
 */
final class FlutterwaveCountries extends Enum
{
    const NIGERIA = 'NG';
    const GHANA = 'GH';
    const KENYA = 'KE';
    const UGANDA = 'UG';
    const SOUTH_AFRICA = 'ZA';
    const TANZANIA = 'TZ';
}
