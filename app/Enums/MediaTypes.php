<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class MediaTypes
 * @package App\Enums
 */
final class MediaTypes extends Enum
{
    public const IMAGES = [
        'image/png',
        'image/jpeg',
    ];

    public const AUDIOS = [
        'audio/ogg',
        'audio/wave',
        'audio/x-wav',
        'audio/x-pn-wav',
        'audio/wav'
    ];
    public const VIDEOS = [
        'video/x-flv',
        'video/mp4',
        'application/x-mpegURL',
        'video/MP2T',
        'video/3gpp',
        'video/quicktime',
        'video/x-msvideo',
        'video/x-ms-wmv'
    ];
}
