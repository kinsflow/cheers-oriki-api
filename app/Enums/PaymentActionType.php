<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class PaymentActionType extends Enum
{
    const FUND_WALLET_ACTION =   'fund_wallet_action';
    const WITHDRAW_WALLET_ACTION =   'withdraw_wallet_action';
}
