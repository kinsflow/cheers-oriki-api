<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class FlutterwaveCurrencies extends Enum
{
    const NIGERIA = 'NGN';
    const GHANA = 'GHS';
    const KENYA = 'KES';
    const UGANDA = 'UGX';
    const SOUTH_AFRICA = 'ZAR';
    const TANZANIA = 'TZS';
}
