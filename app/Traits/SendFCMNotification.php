<?php
namespace App\Traits;

use App\Models\Friend;
use App\Models\User;
use App\Notifications\Friends;

trait SendFCMNotification {
    public function sendMultipleDevice(string $title, string $body, string $tag)
    {
        $server_key = env("FIREBASE_SERVER_KEY");
        $header = [
            "Authorization: Key=" . $server_key,
            "Content-Type: Application/json"
        ];
        $data = [
            "title" => $title,
            "body" => $body,
            "image" => asset('images/cheers-logo.png')
        ];

        $mob_user = User::all();
        $tokens = [];
        for ($i = 0; $i < sizeof($mob_user); $i++) {
            $tokens[$i] = $mob_user[$i]->device_token;
        };

        $payload = [
            "tag" => $tag,
            "registration_ids" => $tokens,
            "notification" => $data,
        ];
        $this->sendPushNotification($payload, $header);

    }
    public function sendToFriendsDevice($title, $body, $tag, $user_id)
    {
        $server_key = env("FIREBASE_SERVER_KEY");
        $header = [
            "Authorization: Key=" . $server_key,
            "Content-Type: Application/json"
        ];
        $data = [
            "title" => $title,
            "body" => $body,
            "image" => asset('images/cheers-logo.png')
        ];
        $friendsId = [];
        $tokens = [];
        $user_friends = Friend::where('first_user', $user_id)->orWhere('second_user', $user_id)->get();
        foreach($user_friends as $friends)
        {
            if($friends->first_user == $user_id)
            {
                $friendsId[] = $friends->second_user;
            }else{
                $friendsId[] = $friends->first_user;
            }
        }
        foreach($friendsId as $friendId)
        {
            $user = User::where('id', $friendId)->first();
            $tokens[] = $user->device_token;
        }
        $payload = [
            "tag" => $tag,
            "registration_ids" => $tokens,
            "notification" => $data,
        ];
        $this->sendPushNotification($payload, $header);
    }


    public function sendSingleDevice($title, $body, $tag, $user_id)
    {
        $server_key = env("FIREBASE_SERVER_KEY");
        $header = [
            "Authorization: Key=" . $server_key,
            "Content-Type: Application/json"
        ];
        $data = [
            "title" => $title,
            "body" => $body,
            "image" => asset('images/cheers-logo.png')
        ];
        $user = User::find($user_id);

        $payload = [
            "tag" => $tag,
            "registration_ids" =>[$user->device_token],
            "notification" => $data,
        ];
        $this->sendPushNotification($payload, $header);
    }

    public function sendPushNotification(array $payload, array $header)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => $header
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);

        curl_close($curl);
        return $response;

    }
}
