<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait SendApiResponse
{
    /**
     * Handles response for successful requests
     *
     * @param $data
     * @param string|null $message
     * @param bool $paginatedData
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($data, string $message = null, $code = Response::HTTP_OK)
    {
        $array = [
            'status' => $code,
            'message' => $message ?? 'Request Successful',
            'data' => $data
        ];

        return response()->json($array, $code);
    }

    /**
     * Handles response for failed requests
     *
     * @param string $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function failureResponse(string $message, int $code = 400)
    {
        return response()->json([
            'status' => $code,
            'message' => $message ?? 'Request failed'
        ], $code);
    }
}
